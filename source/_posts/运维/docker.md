---
title: docker
p: /运维/docker
date: 2017-08-02 21:45:18
categories: 运维
author: ZSP
tags:
    - docker
    - 开源项目
    - 运维
cover_picture: ../images/logo/DockerLogo.jpg

---

## 1.基础知识

1. 容器之间相互隔离,类似于沙盒机制

### 1.镜像分类

1. 中间层镜像:镜像之间可以复用的部分

   ```dockerfile
   docker image ls -a//查看所有
   ```

2. 虚悬镜像:当镜像打补丁时,再次pull或者build时,新镜像就会替换旧的镜像名,旧镜像名称就会变成\<none>;

   ```docker
   docker image prune//删除
   ```

### 2.待补充

## 2.Docker操作

### 1.安装docker

官方[安装文档](https://docs.docker.com/install/linux/docker-ce/centos/),官方文档建议使用yum安装,推荐使用docker社区版本

1. 卸载旧版本:安装过旧版本的执行

   ```linux
   sudo yum remove docker \
   docker-client \
   docker-client-latest \
   docker-common \
   docker-latest \
   docker-latest-logrotate \
   docker-logrotate \
   docker-engine
   ```

2. 安装依赖包

   ```linux
   sudo yum install -y yum-utils \
   device-mapper-persistent-data \
   lvm2
   ```

3. 设置源

   /etc/yum.repos.d/下查看

   ```linux
   sudo yum-config-manager \
   --add-repo \
   https://download.docker.com/linux/centos/docker-ce.repo
   ```

4. 安装docker

   1. 最新版

      ```linux
      sudo yum install -y docker-ce docker-ce-cli containerd.io
      ```

   2. ~~安装特定版本~~

      1. 查看版本

         ```linux
         yum list docker-ce --showduplicates | sort -r
         ```

      2. 安装特定版本

         ```linux
         sudo yum install docker-ce-版本号 docker-ce-cli-版本号 containerd.io
         ```

### 2.配置docker源

1. 配置docker源~~[见[文档](https://docs.docker.com/config/daemon/)另一种方法和出现冲突处理]~~

   新建/etc/docker/daemon.json,配置腾讯云docker加速源

   ```json
   {
       "registry-mirrors":["https://mirror.ccs.tencentyun.com"]
   }
   ```

2. 重启服务

   ```linux
   systemctl restart docker-ce
   ```

### 2.卸载Docker

1. 删除docker包

   ```linux
   yum remove docker-ce
   ```

2. 删除docker目录

   ```linux
   rm -rf /var/lib/docker
   ```


## 3.Docker命令

容器=资源+镜像;镜像:原始包

### 1.镜像

1. 下载镜像

   ```docker
   docker pull [作者名/]镜像名[:版本]
   ```

2. 查看所有镜像

   ```linux
   docker image ls
   ```

   ```dockerfile
   docker images
   ```

3. 查看镜像层数

   ```linux
   docker history tomcat[:版本]
   ```

4. 查看镜像详情

   ```linux
   docker inspcet tomcat
   ```

5. 删除镜像/容器

   ```linux
   docker image rm tomcat
   ```

   ```dockerfile
   docker rmi tomcat
   ```

6. 删除虚悬镜像

   ```linux
   docker image prune
   ```

7. 导出镜像

   ```linux
   docker [image] save tomcat > tomcat.tar
   ```

8. 导入镜像

   ```linux
   docker [image] load < tomcat.tar
   ```

### 2.容器

1. 创建容器

   1. 基础命令

      ```dockerfile
      docker [container] run -参数 
      ```

   2. 参数

      run的参数

      | 参数                 | 作用           |                           |
      | -------------------- | -------------- | ------------------------- |
      | -i                   | 交互式         |                           |
      | -t                   | 伪终端         |                           |
      | -d                   | 后台运行       |                           |
      | --name               | 设置容器名     |                           |
      | --rm                 | 退出时删除容器 |                           |
      | -p 8080:8080         | 指定端口号     | 1:主机端口号;2:容器端口号 |
      | -v 主机目录:容器目录 | 数据卷         |                           |
      |                      |                |                           |

   3. 一般使用

      ```dockerfile
      docker [container] run -itd --name 名字 镜像名
      ```

2. 导入导出容器

   1. 查看正在运行的容器

      显示:容器id、镜像名、COMMAND 、创建时间 、 STATUS 、端口号/协议、容器名

      ```linux
      docker ps
      ```

   2. 导出容器[可导出运行]

      ```linux
      docker [image] export 容器id/容器名 镜像名.tar
      ```

   3. 导入容器[可指定名称]

      ```linux
      docker [image] import 镜像名.tar [重命名:建议原名称]
      ```

3. 打标记

   用之前的镜像创建有个标记不同的镜像

   ```linux
   docker [image] tag tomcat:1 tomcat:标记
   ```

4. 进入关闭容器

   exit(ctrl+z):关闭容器,ctrl+p+q正常退出

   ```dockerfile
   docker [container] attach 容器名/id 
   ```

5. 启动容器

   ```linux
   docker [container] start 容器名
   ```

6. 关闭容器

   ```docker
   docker [container] stop 容器名
   ```

7. 删除容器

   ```dockerfile
   docker [cintainer] rm 容器名
   ```

8. 删除停止状态的容器

   ```docker
   docker container prune 容器名/id
   ```

9. 进入容器

   ```dockerfile
   docker exec -it 容器名[/容器id bash]
   ```

10. 构建镜像

       ```docekr
    docker build -t 容器名 /dockefile所在目录
       ```

### 3.部署容器

#### 1.mysql

1. 下载mysql镜像

   ```docker
   docker pull mysql:5.7.24
   ```

2. 创建容器

   -v /usr/local/docker/mysql/conf:/etc/mysql \

   ```docker
   docker run -p 3306:3306 --name mysql \
   -v /usr/local/docker/mysql/logs:/var/log/mysql \
   -v /usr/local/docker/mysql/data:/var/lib/mysql \
   -e MYSQL_ROOT_PASSWORD=mysqlYANGTUO2019 \
   -d mysql:5.7.24
   ```

## 4.DockerFile

1. 开头必须是FROM 镜像名
2. WORKDIR:设置工作目录
3. RUN + shell脚本
4. copy:复制

## 5.Docker-Compose

用于批处理run容器

### 1.安装

1. 下载Compose

   或者直接下载上传到/uer/local/bin/docker-compose

   ```docker
   sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   ```

2. 增加可执行权限

   ```docker
   sudo chmod +x /usr/local/bin/docker-compose
   ```

### 2.命令

1. 启动

   ```docker
   docker-compose up
   ```

### 3.yml配置文件

docker-compose.yml

[官方文档](https://docs.docker.com/compose/compose-file/)

3级别的可以单独写在1级别上,用于重命名

| 名称           | 作用                     | 级别 | 注意事项                  |
| -------------- | ------------------------ | ---- | ------------------------- |
| version        | docker-compose.yml版本号 | 1    | 3,兼容之前版本            |
| services       | 配置多个容器             | 1    |                           |
|                | 自定义服务名             | 2    | 无名称可以自己起          |
| image          | 指定镜像                 | 3    |                           |
| restart        | 开机是否启动             | 3    | always:总是启动/no:不启动 |
| container_name | 容器名                   | 3    |                           |
| ports          | 端口映射                 | 3    |                           |
| volumes        | 数据卷                   | 3    |                           |
| environment    | 环境变量                 | 3    | 查看hub文档有相关说明     |
| command        | 初始化                   | 3    | --                        |

### 4.部署容器

#### 1.gitLab

1. 相关资料

   这里下载的是gitLab的汉化版,并非官方版,在dockerHub上搜索gitlab-ce-zh搜索选择汉化版本;

   [gitlat中文社区文档](https://hub.docker.com/r/gitlabcezh/gitlab-ce-zh)

   [gitlab-docker官方文档](https://github.com/sameersbn/docker-gitlab/blob/master/README.md)

   [gitlab硬件要求:](https://docs.gitlab.com/ee/install/requirements.html)当前建议8G+7200转机械硬盘或固态硬盘,否则启动和运行都很不理想

2. docker-compose.yml

   ```yml
   version: '3'
   services:
       web:
         image: 'twang2218/gitlab-ce-zh'
         restart: unless-stopped
         hostname: 'www.airbash.cn'
         environment:
           TZ: 'Asia/Shanghai'
           GITLAB_OMNIBUS_CONFIG: |
             external_url 'http://www.airbash.cn/gitlab'
             gitlab_rails['time_zone'] = 'Asia/Shanghai'
         ports:
           - '8080:80'
           - '8443:443'
           - '2222:22'
         volumes:
           - /usr/local/docker/gitlab/config:/etc/gitlab
           - /usr/local/docker/gitlab/data:/var/opt/gitlab
           - /usr/local/docker/gitlab/logs:/var/log/gitlab
   ```