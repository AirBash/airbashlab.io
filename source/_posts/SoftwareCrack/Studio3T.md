---
title: Studio3T破解教程
cover_picture: ../images/logo/Studio3T.png
p: /SoftwareCrack/Studio3T
date: 2019-11-16 13:44:45
tags:
    - 软件破解
    - StudioST
    - MongoDB
categories:
    - 软件破解
---
### 注意事项:
1. 此教程并非真正破解，而是通过重置 studio 3t 的试用时间来实现伪破解。
2. 此教程不适用于2019.2.1以后的版本,点击这里下载[2019.2.1版官方版](https://download.studio3t.com/studio-3t/windows/2019.2.1/studio-3t-x64.zip)
### 方法1:使用脚本

1. 右击新建`txt`文本文件
    
2. 输入以下代码
    

```bat
@echo off
ECHO 重置Studio 3T的使用日期......
FOR /f "tokens=1,2,* " %%i IN ('reg query "HKEY_CURRENT_USER\Software\JavaSoft\Prefs\3t\mongochef\enterprise" ^| find /V "installation" ^| find /V "HKEY"') DO ECHO yes | reg add "HKEY_CURRENT_USER\Software\JavaSoft\Prefs\3t\mongochef\enterprise" /v %%i /t REG_SZ /d ""
ECHO 重置完成, 按任意键退出......
pause>nul
exit
```

3. 将文件命名为`studio3t.bat`
    
4. 破解:
    1. 手动破解直接运行 studio 3T 即可延长使用期限
    2. 自动破解:将`studio3t.bat`文件复制到以下路径,每次系统启动时会自动进行脚本 
    `C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp`
    

![](http://upload-images.jianshu.io/upload_images/17559159-fca6f632db6c1a21.jpg)  

### 方法2:修改注册表

1. 打开注册表:按住运`win 和 r` 键输入 `regedit`,回车;
    
2. 找到以下路径  
    `HKEY_USERS\S-1-5-21-xxxxxxxxxxxxxx\SOFTWARE\JavaSoft\Prefs\3t\mongochef\enterprise`
    
3. 将除了 `installation-date-2019.2.1`（这个是软件安装日期的时间戳）**这个项以外的项数据修改为空**，再次打开软件就又是 30 天试用了  
    
    ![](http://upload-images.jianshu.io/upload_images/17559159-1c9957280bfd8ab5.JPG)