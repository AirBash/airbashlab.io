---
title: 安装使用Redis
cover_picture: ../images/logo/Redis.jpg
P: 数据库/Redis/installAndUse
date: 2018-11-15 19:50:53
tags:
   - 数据库
   - Redis
categories:
   - 数据库
---
### 1.数据库管理

#### 1.编译安装

1. 下载redis

   ```shell
   wget -P /usr/local/src/ http://download.redis.io/releases/版本
   ```

2. 解压redis

   ```bash
   tar -xzf redis文件
   ```

3. 编译redis

   ```shell
   cd redis/src
   make
   ```

4. 卸载redis:直接删除

#### 2.环境变量

1. 打开配置文件

   ```shell
   vim /etc/profile
   ```

2. 修改配置文件:文件最后增加

   ```makefile
   #Redis
   export REDIS_HOME=/usr/local/src/redis-5.0.5/
   export PATH=$PATH:$REDIS_HOME/src
   ```

3. 执行修改

   ```shell
   source profile
   ```

4. 重新登录

#### 3.启动连接

1. 运行redis

   ```shell
   $ redis-server [配置文件]
   ```

2. 连接redis

   可以直接写成`redis-cli`代表本地连接

   ```shell
   $ redis-cli -h host -p 6379 [-a 密码]
   ```

3. 关闭redis

   ```shell
   $ redis-cli shutdown
   ```

   通过端口号[不推荐]

   ```shell
   ps -ed | grep 名称
   kill -9 端口号
   ```