---
title: Redis
cover_picture: ../images/logo/Redis.jpg
p: /数据库/Redis
date: 2019-11-01 18:31:03
tags:
   - 数据库
   - Redis
categories:
   - 数据库
---

## 1.基础知识

1. 版本
   1. 3.x之前不支持分布式安装
   2. 3.x之后支持分布式安装

2. 优缺点
   1. 单个redis容易发生单点故障,一台服务器很难处理大量内存.
   2. redis可用的存储空间有限,即使是打野不能用


### 1.基础配置

./redis/redis.conf

| 名称(默认)                  | 作用                       | 行号    | 注意事项                                                     |
| --------------------------- | -------------------------- | ------- | ------------------------------------------------------------ |
| bind   127.0.0.1            | 可以连接的地址             | 69      | 注释掉设置为远程登录                                         |
| port 6379                   | 端口号                     | 93      | 自行设置                                                     |
| timeout 0                   | 设置超时断开时间           | 113     | 自行设置                                                     |
| daemonize no                | 设置为守护线程             | 136     | yes:守护线程                                                 |
| pidfile /var/run/端口号.pid | 设置pid的文件              | 159     | 只有设置为守护线程才生效                                     |
| loglevel verbose            | 日志级别                   |         | debug、verbose(默认)、notice、warning                        |
| logfile   日志文件位置      | 设置日志保存文件           | 172     | 设置为守护线程默认为:/dev/null(黑洞)                         |
| databases   16              | 设置存储库数量             | 187     | 默认16个                                                     |
| save 10 1                   | 10秒钟有1个key持久化到硬盘 | 219-221 | 三个条件满足其中一个就进行RDB持久化                          |
| rdbcompression   yes        | 本地存储数据是否进行压缩   | 252     | yes:开启默认开启                                             |
| dbfilename 持久化文件       | 设置持久化备份的库文件     | 254     |                                                              |
| dir 指定数据存储位置        | 指定数据库保存的位置       | 264     | 默认redis目录下                                              |
| requirepass  foobared       | 设置密码                   | 508     | 默认注释(redis3.0之后必须设置),通过"-a 密码"进行连接,明文书写即可,java通过auth连接 |
| maxclients                  | 设置并发线程连接数         | 540     | 默认注释没有限制                                             |
| maxmemory\<bytes>           | 指定Redis最大内存          | 567     | 默认注释,默认值256M(建议1g分配256-512)                       |

### 2.内存淘汰机制

只需要配置一个模式

| 名称            | 作用                                      | 注意事项               |
| --------------- | ----------------------------------------- | ---------------------- |
| volatile-lru    | 删除最不常用的超时数据                    | 后面配置超时时间       |
| allkeys-lru     | 查询所有key中最近最不常使用的数据进行删除 | 常被配置的模式         |
| volatile-random | 随机删除超时数据                          | 后面配置超时时间       |
| allkeys-random  | 查询所有数据后随机删除                    |                        |
| volatile-ttl    | 查询所有超时数据,排序后删除               | 后面配置超时时间       |
| noeviction      | 不进行处理,内存溢出时报错                 | 默认配置               |
| volatile-lfu    | 在所有超时数据中驱逐使用频率最少的键      | 4.0新功能              |
| allkeys-lfu     | 在所有键中驱逐使用频率最少的键            | 4.0新功能     常见配置 |

## 2.redis命令

### 1.共有命令

操作成功几个数据返回几

#### 1.多数据库

1. select:切换数据库

   [0-15]默认16个数据库

   ```redis
   select n
   ```

2. move:移动数据到另一个数据库

   ```redis
   move key n
   ```

3. flushdb:删除当前数据库的所有key

   ```redis
   flushdb
   ```

4. flushall:删除每个数据库的所有key

   ```redis
   flushall 
   ```

#### 2.其他

1. 查看所有key

   ```redis
   keys *
   ```

2. 删除key

   ```redis
   del x [y..]
   ```

3. 检测是否含有key

   ```redis
   exists x [y..]
   ```

4. 清除时间

   1. 设置清除时间

      1. 秒

         ```redis
         expire x 10
         ```

      2. ~~毫秒~~

         ```redis
         pexpire x 1000
         ```

   2. 查看清除时间

      1. 返回值

         | 返回值 | 作用     | 注意事项 |
         | ------ | -------- | -------- |
         | -1     | 永久有效 |          |
         | -2     | 无效     |          |
         | >0     | 剩余时间 |          |

      2. 秒

         ```redis
         ttl x
         ```

      3. 毫秒

         ```redis
         pttl x
         ```

   3. 设置为永久有效

      ```redis
      persist x
      ```

5. 返回随机KEY[不常用]

   ```redis
   random key
   ```

6. 重命名KEY

   ```redis
   rename x y
   ```

7. 查看数据类型

   ```java
   type x
   ```

8. 序列化

   ```redis
   dump x
   ```

### 2.String

最大的key为512mb,不会发生编码异常,在客户端编解码

用途:用户限制登录

1. set增加数据

   可以覆盖之前的数据和类型

   ```redis
   set str 10
   ```

2. mset批量增加

   ```redis
   mset str1 10 str2 x str3 20
   ```

3. 查看一个数据

   1. 

      ```redis
      get string
      ```

   2. 

      ```java
      key sitring:?[1,2]
      ```

4. 批量查看

   ```java
   mget str1 str2 str3
   ```

5. getrange截取

   ```java
   getRange str n1 n2
   ```

6. getset返回之前的值,设置新值

   ```redis
   getset str 15//输出nil
   getset str 10//输出10
   ```

7. strlen长度

   ```java
   strlen str
   ```

8. append判断追加

   key存在时追加值,key不存在时赋值

   ```redis
   append str 10
   ```

9. setnx 判断赋值

   key存在时不进行操作,key不存在时赋值

   ```redis
   setnx str 10;
   ```

10. 加减操作

   只能对String类型的数值进行操作,以0开头的数值视为字符串,不书写n时为加减1,0不存在时从0开始计算

   1. 加1

      ```redis
      incr string
      ```

   2. 减1

      ```redis
      desr string
      ```

   3. 自增n

      ```java
      incrby str n;
      ```

   4. 自减n

      ```redis
      decrby str n;
      ```

5. 添加一个n秒的临时数据

   ```java
   setex str n value
   ```

### 3.Hash

	存储对象,类似于HashMap

1. hset添加对象的一个属性

   ```redis
   hset user:1 id 1
   ```

2. hmset批量添加对象的多个属性

   ```redis
   hmset user:1 id 1 name zhangsan
   ```

3. hget 获取对象的一个属性值

   ```redis
   gset user:1 id
   ```

4. hmset批量获取对象的多个属性值

   ```redis
   hgset user:1 id name
   ```

5. hgetall获取对象的所有属性名和属性值

   ```redis
   hgetall user:1
   ```

6. hdel删除对象的一个或多个属性

   ```redis
   hdel user:1 id [user]
   ```

7. hsetnx存在属性时不操作,不存在属性时添加

   ```redis
   hsetnx user:1 id 1
   ```

8. hexists检测是否存在属性

   返回值存在个数

   ```redis
   hexists user:1 id [name...]
   ```

### 4.List

	用途:做流程
	
	类似于linkedList

1. lpush(从左侧(头部)开始添加,最先添加的是尾部最后一个)

   ```redis
   lpush friend x [x] [x]
   ```

2. rpush(从尾)

   ```redis
   rpush friend x [x] [x]
   ```

   --------------------------------------------------

3. rpushx:最右侧添加(不受1/2的影响)

   ```redis
   rpushx key x
   ```

4. lpushx:最左侧添加(不受1/2的影响)

   ```redis
   lpushx key x
   ```

5. lrange:查看n1-n2的值(可以实现分页)

   负数代表从后开始数第几个,例如-1代表最后一个

   ```redis
   lrange friend n1 n2
   ```

6. lpop:从头部弹出,弹出就没了(删除)

   ```redis
   lpop friend
   ```

7. rpop:从尾部弹出

   ```redis
   rpop friend
   ```

8. brpop:从头部弹出,没值的话等待1000秒,有值就弹出(等待时代表全部弹出,这个key就被删除了,需要重新赋值)

   ```redis
   blpop key 1000
   ```

9. blpop:从尾部弹出,如果在1000秒之后有值就弹出

   ```redis
   blpop key 1000
   ```

10. rpoplpush:从friend1右侧弹出从friends头部插入

    ```redis
    rpoplpush friend1 friend2
    ```

11. lset:更改下标为n的key的值为x

    ```redis
    lset key n x
    ```

12. linset:将x插入下标为n的key的前面或后面

    ```redis
    linsert key before|after n x
    ```

13. llen:长度

    ```redis
    llen key
    ```

14. lindx:显示下标为n的key值

    ```redis
    lindex key n
    ```

### 5.Set

类似于hashtable:无序不重复

1. sadd:添加数据

   ```redis
   sadd key a b c
   ```

2. scard:获取数据数

   ```redis
   scard key
   ```

3. smembers:获取所有数据

   ```redis
   smembers key
   ```

4. sismember:查询是否存在a数据

   ```redis
   sismember key a
   ```

5. srandmember:返回集合中一个或n个数据

   ```redis
   srandmember key [n]
   ```

6. srem:删除key中的一个或多个数据

   ```redis
   srem key a [b]
   ```

7. spop:随机删除key中一个或n个数据并返回删除的值

   ```redis
   spop key [n]
   ```

8. smove:将一个数据移动到另一个key

   将key1中的a移动到key2中

   ```redis
   smove key1 key2 a
   ```

9. 差集:

   1. sdiff:查询交集之外的不相同的数据

      ```redis
      sdiff key1 key2 [key3]
      ```

   2. sdiffstore:脚趾之外的不同的数据复制到key中

      ```redis
      sdiiffstore key key1 key2 [key3]
      ```

10. 交集:

    1. sinter:查询共有数据

       ```redis
       sinter key1 key2 [key3]
       ```

    2. sinterstore:共有数据并复制到key中

       ```redis
       sinterstore key key1 key2 [key3]
       ```

11. 并集:

    1. sunion:查询所有数据

       ```redis
       sunion key1 key2 [key3]
       ```

    2. sunionstore:所有数据并复制到可以中

       ```redis
       sunionstore key key1 key2 [key3]
       ```

### 6.Zset

用途:排行榜

有序集合且不允许重复,但是通过一个double分数来排列顺序,分数可以重复

1. zadd:增加一个或多个数据

   ```redis
   zadd key 分数 a [分数 b]
   ```

2. zcound:查看分数1-分数2的成员个数

   ```redis
   zcard key 分数1 分数2
   ```

3. zrange:通过从低到高查看下标0-n之间的数据

   ```redis
   zrage key 0 n
   ```

4. zrevrange:通过从高到第查看下标0-n之间的数据

   ```redis
   zrevrange key 0 n
   ```

5. zrem:删除数据

   ```redis
   zrem key a [b]
   ```

6. zremrangebybank:删除下标0-n之间的数

   ```redis
   zremrangebybank key 0 n
   ```

7. zremrangebyscore:删除分数之间的数

   ```redis
   zremrangebycore key 分数1 分数2
   ```

## 3.redis操作

### 1.发布订阅

1. 作用:

   订阅频道,从而接收数据

2. subscribe:订阅频道:处于接收状态

   ```redis
   subscribe 频道 [频道1]
   ```

3. publish:向指定频道发送信息

   ```redis
   publish 频道 信息
   ```

4. unsubscribe:退订频道

   ```redis
   unsubcribe
   ```

5. punsubscribe:退订所有频道

   ```redis
   punsubscribe
   ```

### 2.事务

​	用途:一组行为执行时:秒杀活动

1. multi:开始事务

   ```redis
   multi
   ```

2. exec:执行

   数据出错:只有出错的语句不会被执行

   语法错误(报告错误):都不会被执行,会自动回滚

   ```redis
   exec
   ```

3. discard:回滚(放弃执行)

   ```redis
   discard
   ```

4. watch:

   1. 监视一个或多个key,如果这个key在事务期间被更改,那么当`exec`时就会回滚,并取消监视
   2. 在`multi`之前执行,如果执行`exci`或`discard`就会自动取消监视

   ```redis
   watch key [key]
   ```

5. unwatch:取消所有监视

   ```redis
   unwatch [key] [key]
   ```

### 4.持久化 

#### 1.RDB

1. 基础知识:

   1. 默认开启

   2. 定期将内存中的数据做成快照保存在磁盘中

   3. 默认存储在redis目录下,名为dump.rdb

2. 优缺点

   1. 优点:保存和读取数据都很快

   2. 缺点:
      1. 小机器不适合做快照
      2. 在非正常关机时无法保存

3. 快照条件:

   1. 执行shutdown正常关闭时
   2. 执行flushall删除所有key时
   3. 在指定时间间隔内
   4. 执行save命令

4. 恢复rdb:

   将dump.rdb文件拷贝到redis下的bin目录即可

#### 2.AOF

1. 基础知识:

   1. 执行操作之后会将操作写在日志中,如果出现问题可以回滚

   2. 每做一次写操作都会,就会将操作追加到appendoly.aof中,当redis重启时就会重新读取整个数据库

2. 启动AOF,修改配置文件:

   1. 启动AOF:699默认no

      ```conf
      appendonly yes
      ```

   2. 指定存储文件名:703

      ```conf
      appendfilename "appenonly.aof"
      ```

   3. 指定跟新日志条件:728默认被注释

      ```conf
      appendfsync 条件
      ```

      1. always:写操作一次就写入一次:
      2. everysec:每秒钟写入一次:性能和持久化居中
      3. no:完全依赖os,性能最好,持久化没保证

   4. 配置重写机制

      当AOF文件大小是上次rewrite后大小的一倍且文件大于64M时触发。一般都设置为3G，64M太小了。

      ```conf
      auto-aof-rewrite-percentage 100
      auto-aof-rewrite-min-size 64mb
      ```

3. 恢复AOF

   1. ~~直接复制到bin目录下~~

   2. 通过命令

      ```redis
      redis-check-aof --fix appendonly.aof
      ```

### 5.数据一致性

#### 1.实时同步

数据库与redis

@cacheable:查询时使用,注意Long类型需转换为String类型,否则会抛异常

@CatchPut:更新时使用,使用此注解,一定会从DB上查询数据

@CacheEvicat:删除时使用;

@Caching:组合用法:

#### 2.异步队列

消息中间件:kafka

#### 3.其他

1. udf自定义函数
2. 脚本

#### 4.相关知识点

1. 数据库穿透:

   可能查询到的是null数据,null数据不会写入redis缓存,而是返回查询失败,更多的人访问最后导致宕机,因此可以设置如果数据库查询到的是null的话就添加key但是值是null;

2. 雪崩:

   缓存集中在同一时间段失效,导致大量用户进入数据库查询产生数据穿透

   1. 分析用户行为,找到合适的时间点失效
   2. 加锁或者队列
   3. 缓存预热

3. 热点key:

#### 5.主从复制

可以使用阿里插件:canal也可以用下列方法

1. 修改从服务器redis配置文件

   1. port:6380(重新修改端口号):此端口是从服务器的端口号
   2. slaveof 127.0.0.1 6379(指定主服务器和端口号用于连接主服务器)

2. 或者使用命令切换主从服务器

   1. slaveof ip地址 端口号 变回从服务器:只能进行查
   2. slaveof on one 变回主服务器

3. slavef 查询新的master

   slaveof new master

#### 6.哨兵模式

## 4.Cluster集群

### 1.基础知识

1. 无中心,无需连接所有的服务器

2. 必须要3master+3slaveof才能配置成集群

3. 共有0-16383共16384个hash曹,个人服务器均分

4. 容错性(投票):当一个服务器在半数以上的服务器无法连接时视为当前无服务挂掉

5. 节点分配:A(0-5460)B(5461-10922)C(10923-1638)

   如果增加新的服务器则分别从ABC的前面出去一部分最后达成均分

6. 真集群和假集群以下为假集群的创建规则

### 2.搭建流程

#### 1.修改配置文件

1. 复制配置文件

   1. 创建Redis节点安装总目录

      ```linux
      mkdir /usr/local/redis_cluster
      ```

   2. 创建Redis节点安装目录(复制5份redis配置文件(7001-7005))

      ```linux
      cp `~/redis.conf /usr/local/redis_cluster/7001/
      ```

2. 修改配置文件

   | 参数                               | 作用                                               | 行号 |
   | ---------------------------------- | -------------------------------------------------- | ---- |
   | port 7001                          | 修改端口号,6个服务器不同                           | 92   |
   | pidfile /var/run/redis-7001.pid    | 修改pid进程文件.默认以pid命名名称                  | 158  |
   | logfile 存放地址/redis.log         | 修改日志文件地址,可不修改                          | 171  |
   | dir 存放地址                       | 修改数据文件存放地址,以端口号为目录名区分,可不修改 | 263  |
   | cluster-enable yes                 | 启用集群                                           | 832  |
   | cluster-config-file node-7001.conf | 修改节点的配置文件                                 | 840  |
   | cluster-node-timeout 15000         | 配合集群节点的超时时间,使用默认                    | 846  |

3. 启动并查看

   1. 启动5份redis

      ```redis
      redis-server /配置文件
      ```

   2. 查看是否启动

      ```linux
      ps -ef |grep redis
      ```

   3. 关闭reids

      ```redis
      redis-cli -p 7001 shutdown
      ```

#### 2.安装软件

##### 1.ruby

redis-trib.rb的依赖

建议编译安装,而不是下列方法

```linux
yum -y install ruby ruby-devel rubygams rpm-build
```

##### 2.rvm

用于更新ruby,官方文档:https://www.rvm.io/rvm/install

stable:最新版 master:最新开发版 

1. 安装rvm官方的两个gpg秘钥

   1. ~~标准方法(经常失败:原因未知)~~

      ```linux
      gpg2 --keyserver hkp：//pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
      ```

   2. 替代方法

      ```linux
      curl -sSL https://rvm.io/mpapis.asc | gpg --import  - 
      curl -sSL https://rvm.io/pkuczynski.asc | gpg --import  - 
      ```

2. 安装rvm

   1. ~~标准安装~~

      ```linux
      \curl -sSL https://get.rvm.io | bash -s stable
      ```

   2. 使用ruby安装rvm

      ```linux
      \curl -sSL https://get.rvm.io | bash -s stable --ruby
      ```

3. 检验安装情况

   ```redis
   find / -name rvm -print
   ```

4. 加载配置文件

   1. 普通加载

      ```linux
      source /usr/local/rvm/scripts/rvm
      ```

   2. ~~设置环境变量(官方脚本:全局)~~

      ```linux
      source /etc/profile.d/rvm.sh
      ```

5. 升级rvm

   ```linux
   rvm get stable
   ```

6. 查看ruby版本

   ```linux
   rvm list known
   ```

7. 安装最最新版ruby

   ```linux
   rvm install 2.6.3
   ```

8. 使用一个ruby版本

   ```linux
   rvm use 2.6.3
   ```

9. 设置默认ruby版本

   ```linux
   rvm use 2.6.3 --default
   ```

10. 查看ruby版本

    ```linux
    ruby --version
    ```

11. 安装redis与ruby整合工具

    ```linux
    gem isntall redis
    ```

#### 3.创建启动

1. 创建redis_cluster

   1. 3.0-4.0

      ```redis
      redis-trib.rb create --replicas 1 127.0.0.1:7001 127.0.0.1:7002 127.0.0.1:7003 127.0.0.1:7004 127.0.1:7005
      ```

   2. 5.0:不需要安装ruby

      ```redis
      redis-cli --cluster create 127.0.0.1:7000 127.0.0.1:7001 127.0.0.1:7002 127.0.0.1:7003 127.0.0.1:7004 127.0.0.1:7005 --cluster-replicas 1
      ```

   先出的结果为主,输入yes

2. 连接reids

   -c:连接集群 -p:访问7001

   ```linux
   redis-cli -h 127.0.01 -c -p 7001
   ```

3. 查看节点测试:redis命令

   ```redis
   info replication
   ```

4. 查看所有节点信息:redis命令

   每个节点都有一个id,用节点来对应,可以更改端口号

   主用来set,从用来get,若果set就会跳到get中

   ```redis
   cluster nodes
   ```

#### 4.集群管理

1. 添加单个集群

   ```linux
   add-node
   ```

2. 删除单个集群

   ```linux
   del-node
   ```

3. 删除全部集群

   1. 关闭所有集群
   2. 删除nodes-port.conf配置文件
   3. 重建集群

### 4.相关方法

1. 新建对象

   ```java
   @Autowired
   	private RedisTemplate<String,String> redisTemplate;
   	
   	public String getString(String username) {
   		ValueOperations<String, String> string = redisTemplate.opsForValue();
   		if(redisTemplate.hasKey(username)) {//redis中有,直接输出
   			System.out.println("在redis中查找:");
   			return string.get(username);
   		}
   		else {//redis中没有数据,在mysql中查找后赋值(模拟),并输出
   			String age="15";
   			string.set(username, age);
   			System.out.println("在mysql中查找:");
   			return age;
   		}
   	}
   ```

2. 语法规律

   公用redis语法对应redisTemplate静态方法

   私有redis语法对应redisTemplate.ospFor***()