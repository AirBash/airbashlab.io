---
title: 帮助信息
cover_picture: ../images/logo/PowerShell.jpg
p: PowerShell/getHelp
date: 2018-11-18 00:10:45
categories: PowerShell
tags:
    - PowerShell
    - 运维
---
1. 查看官方文档

   [PowerShell官方英文文档](https://docs.microsoft.com/en-us/powershell)(中文文档翻译不全)--->示例脚本

2. 下载帮助模块

   - 当执行`update-help`后powershell会报下述错误:

     ```error
     update-help : 无法更新带有 UI 区域性 {zh-CN} 的模块“AppvClient, Defender, HostComputeService, HostNetworkingService, M
     icrosoft.PowerShell.ODataUtils, Microsoft.PowerShell.Operation.Validation, UEV, Whea, WindowsDeveloperLicense”帮助: 无
     icrosoft.PowerShell.ODataUtils, Microsoft.PowerShell.Operation.Validation, UEV, Whea, WindowsD然后重试该命令。
     eveloperLicense”帮助: 无
     法连接到帮助内容。存储帮助内容的服务器可能不可用。请确认该服务器可用，或等到该服务器重新联机，
     然后重试该命令。
     所在位置 行:1 字符: 1
         + FullyQualifiedErrorId : UnableToConnect,Microsoft.PowerShell.Commands.UpdateHelpCommand
     
     update-help : 无法更新带有 UI 区域性 {zh-CN} 的模块“WindowsUpdateProvider”帮助: 在 HelpInfo XML 文件中检索不到 UI 区
     域性 zh-CN。确保模块清单中的 HelpInfoUri 属性有效或检查网络连接是否正常，然后重试该命令。
     所在位置 行:1 字符: 1
     + update-help
     + ~~~~~~~~~~~
         + CategoryInfo          : ResourceUnavailable: (:) [Update-Help], Exception
         + FullyQualifiedErrorId : UnableToRetrieveHelpInfoXml,Microsoft.PowerShell.Commands.UpdateHelpCommand
     ```

   -  这是由于某些命令的在线文档地址错误，使用`-ErrorAction SilentlyContinue`忽略报错信息并继续执行后续

     ```powershell
     update-help -ErrorAction SilentlyContinue
     ```

3. 查看帮助

   ```powershell
   get-help 命令
   man 命令
   ```