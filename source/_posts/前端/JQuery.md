---
title: JQuery
cover_picture: ../images/logo/JQueryLogo.jpg
p: 前端/JQuery
date: 2019-11-02 20:52:17
tags:
	- 前端
	- Jquery
categories:
   - 前端
---

### 一.基础知识

#### 1.安装使用

- 离线使用

1. 从网上下载jQuery的JS文件,放到项目目录

2. 使用时导入到需要使用的文件中(导入行内不能写代码)

   ```javascript
   <script type="text/javascript" src="../js/jquery-1.4.2.js"></script>
   ```

- 在线使用

  ```javascript
  <script type="text/javascript" src="地址"></script>
  ```

#### 2.注意事项

- 优势
  - 相对于js能更好的选择元素,修改css
  - 当一个网站下载完以后,另一个网站可以复用

- 详细介绍:
  - jQuery是一个用js写的js框架
  - 选择器时jQuery的基础

- 创建新元素时命名为$(),或者jQuery()

#### 3.对象转换

jQ对象和JS对象之间的方法不同互相使用

- js转换为jQuery:

```javascript
js转jq   var jq = $(js);
```

- jQuery转换为js:

```javascript
jq转js  var js = jq[0];
```

#### 4.区别/方法

| jq代码 | js代码   |
| ------ | -------- |
| val()  | value    |
| eq(n)  | 遍历     |
| each() | 循环遍历 |
| load   | onload   |

#### 5.遍历数组

- jQuery方法调用

  ```javascript
  var a = [1, 2, 3, 4];
  $.each(a, function(key, val) {//以jQuery对象的方法调用，兼容性好;
      console.log(a[key] + '下标为' + key + '值为' + val);
  });
  ```

- 对象转换调用

  ```javascript
  //也可以用$(a)将a转化为jquery对象，然后以下列方式的形式调用
  $(a).each(function(key,val){
      console.log(a[key] + '下标为' + key + '值为' + val);
  });
  ```

- 遍历对象时

  $("img").eq(0)=$(this)=$(val)

### 二.选择器(获取元素)

#### 1.基本选择器

| 代码                   | 选择器                                |
| ---------------------- | ------------------------------------- |
| $("*")                 | 任意元素选择器                        |
| $("标签名")            | 标签名选择器                          |
| $("#id名")             | Id选择器                              |
| $(".class名")          | class选择器                           |
| $("标签名,#id,.class") | 分组选择器                            |
| $("标签名.class名")    | 元素选择器(标签名为XXX且class名为XXX) |

#### 2.层级选择器

| 代码          | 选择器                               |
| ------------- | ------------------------------------ |
| $("div span") | 匹配所有div下的span元素 包含所有后代 |
| $("div>span") | 匹配所有div下的span子元素            |
| $("div+span") | 匹配所有div后面的第一个弟弟元素      |
| $("div~span") | 匹配所有div后面所有的span弟弟们元素  |

#### 	2.2层级方法

不包括自身/当选择器全部选择,然后查找兄弟元素,第一个不被选择

| 代码                      | 方法                          |
| ------------------------- | ----------------------------- |
| $("#abc").siblings("div") | 匹配id为abc元素的所有兄弟元素 |
| $("#abc").next("div")     | 匹配id为abc元素的弟弟元素     |
| $("#abc").nexAll()        | 匹配id为abc元素的弟弟元素们   |
| $("#abc").prev("div")     | 匹配id为abc元素的哥哥元素     |
| $("#abc").prevAll()       | 匹配id为abc元素的哥哥元素们   |

#### 3.过滤选择器

| 代码               | 选择器                          |              |
| ------------------ | ------------------------------- | ------------ |
| $("div:first")     | 匹配所有div元素中的第一个       | 存在单独方法 |
| $("div:last")      | 匹配所有div元素中的最后一个     | 存在单独方法 |
| $("div:even")      | 匹配所有div元素中的偶数个       | 从0开始      |
| $("div:odd")       | 匹配所有div元素中的奇数个       | 从0开始      |
| $("div:eq(n)")     | 匹配第n个div元素                | 存在单独方法 |
| $("div:lt(n)")     | 匹配所有下标小于n的所有div元素  |              |
| $("div:gt(n)")     | 匹配所有下标大于n的所有div元素  |              |
| $("div:not(.abc)") | 匹配所有div中class值不等于abc的 |              |

#### 3.3过滤方法

| 代码                                                         | 实例                                  |
| ------------------------------------------------------------ | ------------------------------------- |
| $("#abc").filter(function(index,ele){index相关flag return true}) | 过滤return为 true的所有div元素        |
| $("#abc").map(function(index,ele){index相关flag return ele}) | 过滤return为ele的div元素              |
| $("#abc").find(".class名")                                   | 匹配class为"class名"的所有div元素     |
| $("#abc").not(".class名")                                    | 匹配class不为"class名"的所有div元素   |
| $("#abc").is(".class名")                                     | 检查class为"class名"的所有div元素     |
| $("#abc").slice(a,b)                                         | 匹配[a,b)div元素,不写b时代表[a,+无限) |
| $("#abc").has(p)                                             | 匹配所有包含p子元素的div              |

#### 4.内容选择器

| 代码                     | 选择器                   |
| ------------------------ | ------------------------ |
| $("div:has(p)")          | 匹配所有包含p子元素的div |
| $("div:empty")           | 匹配所有空的div          |
| $("div:parent")          | 匹配所有非空的div        |
| $("div:contains('abc')") | 匹配所有包含abc文本的div |

#### 5.属性选择器

| 代码               | 选择器                   |
| ------------------ | ------------------------ |
| $("div[id]")       | 匹配所有包含id属性的元素 |
| $("div[id='d1']")  | 匹配所有id等于d1的元素   |
| $("div[id!='d1']") | 匹配所有id不等于d1的元素 |

#### 6.可见选择器

| 代码             | 选择器                |
| ---------------- | --------------------- |
| $("div:hidden")  | 匹配所有隐藏的div元素 |
| $("div:visible") | 匹配所有显示的div元素 |

#### 6.2隐藏显示相关的方法

| 代码                | 选择器       |
| ------------------- | ------------ |
| $("#abc").hide();   | 隐藏         |
| $("#abc").show();   | 显示         |
| $("#abc").toggle(); | 隐藏显示切换 |

#### 7.子元素选择器

| 代码                  | 选择器                                   |
| --------------------- | ---------------------------------------- |
| $("div:nth-child(n)") | 匹配所有是div并且是第n个子元素 //从1开始 |
| $("div:first-child")  | 匹配所有是div并且是第一个子元素          |
| $("div:last-child")   | 匹配所有是div并且是最后一个子元素        |

#### 8.表单选择器

| 代码               | 选择器                           |
| ------------------ | -------------------------------- |
| $(":input")        | 匹配form表单中所有的控件         |
| $(":password")     | 匹配所有密码框                   |
| $(":radio")        | 匹配所有单选框                   |
| $(":checkbox")     | 匹配所有多选框                   |
| $(":checked")      | 匹配所有选中的单选、多选、下拉选 |
| $("input:checked") | 匹配所有选中的单选和多选         |
| $(":selected")     | 匹配所有选中的下拉选             |

### 三.添加元素

- 创建元素

  不立即生效,需要下面三个方法的协助

    ```javascript
  新元素 = $("<div id='xxx'>xxxxx</div>");
    ```

- 添加元素

  ```javascript
  父元素.append(新元素);  //添加到最后面
  父元素.prepend(新元素); //添加到最前面
  ```

- 插入元素

  ```javascript
  兄弟元素.before(新元素);//插入到元素的前面
  兄弟元素.after(新元素);//插入到元素的后面
  ```

- 删除元素

  ```javascript
  被删除元素.remove();
  ```

### 四.写入元素

- 注意事项:

  1. js上这里赋值也是"=",jq变成类似java正常情况;
  2. ()内不写代表取值;
  3. 以下内容跟js一样都添加新元素时删除之前的配置
  4. 创建的html新元素只能添加一遍,第二次添加的话不生效
  5. 可以将创建的新元素1添加到新元素2,然后将新元素2添加到文件中

- 文本相关

  ```javascript
  元素.text("xxx");				//给元素添加文本内容
  var str = 元素.text();		//获取元素的文本内容
  ```

- html相关

  ```javascript
  元素.html("<h1>xxx</h1>");	//给元素添加html内容
  var html = 元素.html();		//获取元素的html内容
  ```

- css样式相关

  ```javascript
  元素.css("样式名称","值"); //给元素添加样式
  元素.css({"css名称1":"值1","css名称2":"值2"}); //给元素添加多种样式
  var x = 元素.css("样式名称"); //获取元素某个样式的值
  ```

#### 属性相关

- 注意事项:

  1. 不删除之前的配置

  2. attr和prop区别

     | attr                                 | prop                   |
     | ------------------------------------ | ---------------------- |
     | 可以获取和添加固定属性和自定义标签   | 只能获取和添加固定属性 |
     | 获取未设置属性返回undefined          | 获取未设置属性返回null |
     | 可以动态获取checked的值(true或false) |                        |

  3. 固定属性:已经html标签

     自定义属性:自己定义的标签

##### 1.attr

- 添加属性

  ```javascript
  元素.attr("属性名","属性值");
  ```

- 获取属性

  ```javascript
  var x = 元素.attr("属性名");
  ```

- 删除属性

  ```javascript
  var.Removeattr("属性名");
  ```

##### 2.prop

- 添加属性

  ```javascript
  元素.prop("属性名","属性值");
  ```

- 获取属性

  ```javascript
  var x = 元素.prop("属性名");
  ```

- 删除属性

  ```javascript
  var.Removeprop("属性名");
  ```


##### 3.class

- 添加class 

  1. 相比较与attr可以分别设置而不用遍历
  2. 不删除之前的配置

  ```javascript
  $("").addClass(function(index ele){
      if(index相关flag){
          return "数值";
      }
  })
  ```

- 删除class

  ```javascript
  $("").removeClass("数值");
  ```

- 判断是否含有class

  1. 相比较与attr可以不写全class名
  2. 用于多class名

  ```javascript
  $("").hasClass("class名");
  ```

##### 4.checked

​	checked或者true

### 五.Cookie

1. 存入cookie

   ```javascript
   $.cookie(数据名, 数据值, {expire: 有效期为多少天});
   ```

2. 使用cookie

   ```javascript
   $.cookie(数据名);
   ```

### 六.事件

| 事件    | 描述 | js        |
| ------- | ---- | --------- |
| click() |      | onclick() |

