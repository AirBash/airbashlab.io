---
title: AJAX
cover_picture: ../images/logo/AJAXLogo.jpg
p: Spring/AJAX
date: 2019-11-02 20:39:45
tags:
   - Spring
   - JAVA
   - 框架
categories:
   - 前端
---

### 1. AJAX

#### 1.基础内容

1. 实现功能:

   在不转发和重发的情况下处理页面(异步)

2. 基础配置:

   - 配置tomcat
   - 设置spring-mvc依赖
   - 字符集.自动查找

#### 2.$.ajax参数

| 参数名            | 内容                                                       | 注意事项         |
| ----------------- | ---------------------------------------------------------- | ---------------- |
| url               | 访问地址                                                   |                  |
| data              | 发送表单数据(不写时代表接收数据)new FormData($("#form"[0]) |                  |
| type              | 请求方法:get/post(与服务器控制层一致)                      |                  |
| dataType          | 服务器return类型:json/xml/text/html                        | 不指定时自动识别 |
| success           | 成功响应时(响应码为2XX时)的处理函数                        |                  |
| error             | 失败时响应(响应吗为2XX以外)的处理函数                      |                  |
| contentType:false | 上传                                                       |                  |
| contentData:false | 上传                                                       |                  |

error在项目中通常只处理3XX错误(400X/500X项目中不应该出现)

#### 3.JQ代码

```javascript
$("#btn-login").click(function(){
	$.ajax({
        "url":"user/login.do",
            "data":"username="+$("#username").val()+"&password="+$("#password").val(),
        "type":"post",
        "dataType":"json",
            "success":function(json){
                if(json.state == 1){
                    alert(json.message);
                }else if(json.state ==2){
                    alert(json.message);
                }else{
                    alert(json.message);
                }
            }
	});
});
```

### 2. JSON

#### 1.语法

1. 标签名\文本都要加双引号
2. 标签名右侧冒号[+文本]
3. 父标签后加大括号
4. 文本中String类型需要加双引号,int类型不需要加双引号

#### 2.与xml的比较

- xml

  ```xml
  <state>1</state>
  <data>
  	<username>root</username>
  	<age>18</age>
  </data>
  ```

- json

  ```json
   {
      "state":1,
  	"data":{
     		"username":"root",
      	"age":18
  	}
  }
  ```

#### 3.Json中的数组

- 数组:

  ```json
  {
  	"state":1,
  	"data": [
  		"Mike", "Jack", "Lucy"
  	]
  }
  ```

- 数组中包含对象

  ```json
  {
  	"state":1,
  	"data": [
  		{
  			"username":"Mike",
  			"age":23
  		},
  		{
  			"username":"Jack",
  			"age":21
  		},
  		{
  			"username":"Lucy",
  			"age":25
  		}
  	]
  }
  ```

#### 4.javascript

json可以被javascript直接识别,`json.parse(str)`

```javascript
var json = {
	"state":1,
	"message":"欢迎使用JSON",
	"data":[
		{
			"username":"Mike",
			"age":23
		},
		{
			 "username":"Lucy",
			 "age":21
		},
		{
			 "username":"jack",
			 "age":22
			 }
		]
};
for(var i=0;i<json.data.length;i++){
	alert(json.data[i].username);
}
```

### 3. springMVC整合

#### 1.服务器响应String类型

##### 1.英文类型

1. controller.java

   只注解@ResponseBody即可,可以和@controller合并为`@RestController`

   ```java
   @Controller
   @RequestMapping("user")
   public class UserController {
   	@RequestMapping("login.do")
   	@ResponseBody//默认不支持utf-8,添加json依赖后支持
   	public String handleLogin(
   		String username, String password) {
   		if ("root".equals(username)) {
   			if ("1234".equals(password)) {
   				return "OK";
   			} else {
   				return "error";
   			}
   		} else {
   			return "error";
   		}
   	}
   }
   ```

2. 测试

   只要是将数据发送给controller并转到croller页面就会发生打印现象

     1. 访问当下地址http://localhost:8080/spring-10-ajax/user/login.do?username=root&password=1234;
     2. from接收后;

 3. 结果

    ​	添加后不转换地址,可以吧return直接输入到页面上

##### 2.中文类型

1. 原理:

   相当于在html添加数据,而html响应头默认为8859-1类型的,因此需要修改默认字符集才能在页面上显示中文,否则乱码

2. 方法1:

   修改@ResponseBody为`@ResponseBody(text/html;charset=UTF-8)`

3. 方法2:

   ```xml
   <mvc:annotation-driven>
   	<mvc:message-converters>
   		<bean class="org.springframework.http.converter.StringHttpMessageConverter">
   			<property name="supportedMediaTypes">
   				<list>
   					<value>text/html;charset=UTF-8</value>
   				</list>
   			</property>
   		</bean>
   	</mvc:message-converters>
   </mvc:annotation-driven>
   ```

#### 2. 服务器响应JSON格式的结果

1. **添加依赖**

   ```xml
   <dependency>
     	<groupId>com.fasterxml.jackson.core</groupId>
     	<artifactId>jackson-databind</artifactId>
     	<version>2.9.8</version>
   </dependency>
   ```

2. **注解驱动**

   - jackson必须在spring.xml中添加驱动才能起作用

   1. 在添加@ResponseBody注解以后只能return英文,注解驱动后可以return中文,并且可以return"json"
   2. 中文原理:将响应头改为:application/json; charset=UTF-8

   ```xml
   <mvc:annotation-driven></mvc:annotation-driven>
   ```

3. **创建实体类**

   ```java
   public class ResponseResult {
   	private Integer state;
   	private String message;
   	// 某些构造方法/SET/GET
   }
   ```

4. **controller类**

   @ResponseBody//启用AJAX:默认不支持utf-8,添加json注解后支持

   ```java
   @Controller
   @RequestMapping("user")
   public class UserController {
   	@RequestMapping("login.do")
   	@ResponseBody//默认不支持utf-8,添加json注解后支持
   	public String handleLogin(
   		String username, String password) {
   		if ("root".equals(username)) {
   			if ("1234".equals(password)) {
   				return new AAA(1,"登录成功");
   			} else {
   				return new AAA(3,"密码错误");
   			}
   		} else {
   			return new AAA(2,"账号错误");
   		}
   	}
   }
   ```

5. **页面显示结果**

   访问user/login.do

   ```json
   {"state":1,"message":"登录成功"}
   ```

6. 转换器

   在响应正文的过程中需要使用转换器Converter:

   返回值类型String:StringHttpMessageConverter转换器

   无法识别时:jackson转换器

### 4.  前端页面

1. 解决方案:

   通过取消form表单的提交功能

   1. 删除form标签
   2. 使用button

2. html

   ```html
   <form method="post" action="handle_login.do">
   	<h1>用户登录</h1>
   	<div>请输入用户名</div>
   	<div><input name="username" /></div>
   	<div>请输入密码</div>
   	<div><input name="password" /></div>
   	<div><input id="btn-login" type="button" value="登录" /></div>
   </form>
   ```

3. jQuery

   ```javascript
   <script type="text/javascript" src="jquery-1.8.3.min.js"></script>
   <script type="text/javascript">
       $("#btn-login").click(function(){
           alert("hello, jquery!");
       });
   </script>
   ```

   写在文档的最后面.或者使用下列代码

   ```javascript
   <script>
   	$(document).ready(function(){
   		$("#btn-login").click();
   	}); //网页全部加载完之后再执行
   </script>
   ```