---
title: 生成sitemap站点目录
cover_picture: ../images/logo/HEXO.jpg
date: 2019-11-06 14:41:07
tags:
	- Hexo
categories:
	- Hexo
---

百度+谷歌都无法搜索到我的博客 不能忍

### 1.先确认博客是否被收录
在百度或者谷歌上面输入下面格式来判断，如果能搜索到就说明被收录，否则就没有。

```shell
	site:site.cn
```
![123](/images/Hexo/GenerateSiteMap/siteGoole.png)

### 2.创建站点地图文件
站点地图是一种文件，您可以通过该文件列出您网站上的网页，从而将您网站内容的组织架构告知Google和其他搜索引擎。搜索引擎网页抓取工具会读取此文件，以便更加智能地抓取您的网站。

- 先安装一下，打开你的hexo博客根目录，分别用下面两个命令来安装针对谷歌和百度的插件
	```shell
	npm install hexo-generator-sitemap --save
	npm install hexo-generator-baidu-sitemap --save
	```

- 在博客目录的_config.yml中添加如下代码
	```yml
	Plugins:
		- hexo-generator-baidu-sitemap
		- hexo-generator-sitemap
	baidusitemap:
    	path: baidusitemap.xml
	sitemap:
    	path: sitemap.xml
	```

在你的博客根目录的public下面发现生成了sitemap.xml以及baidusitemap.xml就表示成功了

### 3.让百度收录我们的博客
1. 验证网站

	- 为什么要验证网站
		站长平台推荐站长添加主站（您网站的链接也许会使用www 和非 www 两种网址，建议添加用户能够真实访问到的网址），添加并验证后，可证明您是该域名的拥有者，可以快捷批量添加子站点，查看所有子站数据，无需再一一验证您的子站点。
	- 如何验证网站
		首先如果您的网站已使用了百度统计，您可以使用统计账号登录平台，或者绑定站长平台与百度统计账号，站长平台支持您批量导入百度统计中的站点，您不需要再对网站进行验证。
		百度站长平台为未使用百度统计的站点提供三种验证方式：文件验证、html标签验证、CNAME验证。
		验证完成后，将会认为您是网站的拥有者。为使您的网站一直保持验证通过的状态，请保留验证的文件、html标签或CNAME记录，会去定期检查验证记录。

2. 访问流程
	1. 访问[百度站长平台](https://ziyuan.baidu.com/site/)
	2. 输入你的网址

![](/images/Hexo/GenerateSiteMap/1.输入网址.png)
	3. 这里推荐使用CNAME验证证,因为这样不影响我们HEXO的操作.
![](/images/Hexo/GenerateSiteMap/3.域名验证.png)

#### 链接提交
上面步骤成功后，进入站点管理，选择网页抓取——链接提交

这里推荐自动推送和sitemap
从效率上来说：

```
主动推送>自动推送>sitemap
```

#### 自动推送
自动推送很简单，就是在你代码里面嵌入自动推送JS代码，在页面被访问时，页面URL将立即被推送给百度
复制代码到下面目录文件里就好添加到下面就行。


```
\themes\xxxx\layout\_partial\after_footer.ejs
```

#### sitemap提交
直接提交就行

如何选择链接提交方式?

1、主动推送：最为快速的提交方式，推荐您将站点当天新产出链接立即通过此方式推送给百度，以保证新链接可以及时被百度收录。
2、自动推送：最为便捷的提交方式，请将自动推送的JS代码部署在站点的每一个页面源代码中，部署代码的页面在每次被浏览时，链接会被自动推送给百度。可以与主动推送配合使用。
3、sitemap：您可以定期将网站链接放到sitemap中，然后将sitemap提交给百度。百度会周期性的抓取检查您提交的sitemap，对其中的链接进行处理，但收录速度慢于主动推送。
4、手动提交：一次性提交链接给百度，可以使用此种方式。

### 4.谷歌收录我们的博客
谷歌操作比较简单，就是向Google站长工具提交sitemap
登录Google账号，添加了站点验证通过后，选择站点，之后在抓取——站点地图——添加/测试站点地图，如下图：

谷歌我提交立马就能搜索到我的博客了，效率很高。