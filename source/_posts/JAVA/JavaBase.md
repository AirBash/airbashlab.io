---
title: JavaBase
cover_picture: ../images/logo/JavaLogo.jpg
p: /JAVA/JavaBase
date: 2019-11-01 19:55:40
tags:
- JAVA
categories:
- JAVA
---

## 1.基础知识

### 1.java专用词

| 名词         | 作用                          | 注意事项                                   |
| ------------ | ----------------------------- | ------------------------------------------ |
| 实例化       | 能new xxxx();                 | 不能实例化的:接口/抽象类                   |
| 引用数据类型 | 除了基本的数据类型以外的类型: | String/接口(API)/数组                      |
| 对象         | new Person();                 | 一个对象可以被多个引用所指向               |
| 引用         | Person person                 | 一个引用可以指向多个对象,全称`引用对象变量 |

### 2.java环境

- 编译过程:

  - 编译期:java源文件,经过编译,生成.class字节码文件
  - 运行期:JVM加载,class并运行.class
  - 跨平台,一次编译到处使用

- 详细:

  - JVM:java虚拟机

    加载.class并运行.class

  - JRE:java运行环境

    JRE=JVM+java系统类型(小零件)

  - JDK:java开发环境

    JDK=JVM+编译运行等命令工具

## 2.数据类型

### 1.基本数据类型

1. 列表

   | 所属类型   | 基本类型 | bit  | byte | 范围                                                         | 注意                           |
   | ---------- | -------- | ---- | ---- | ------------------------------------------------------------ | ------------------------------ |
   | 整数类型   | byte     | 8位  | 1    | -128/127                                                     |                                |
   |            | short    | 16位 | 2    | -32768/32767                                                 |                                |
   |            | int      | 32位 | 4    | -21亿/21亿                                                   | 默认类型                       |
   |            | long     | 64位 | 8    | 灰常大                                                       | 长整型直接量需要在数字后加L或l |
   | 浮点数类型 | float    | 32位 | 4    | 可能出现舍入误差,单精度                                      |                                |
   |            | double   | 64位 | 8    | 可能出现舍入误差,双精度                                      | 默认类型                       |
   | 字符型     | char     | 16位 | 2    | 采用Unicode字符集编码，一个字符对应一个码,表现形式为字符char，但本质上是码int(0到65535之间)   (ASCII:   'a'--97  'A'--65  '0'--48) |                                |
   | 布尔型     | boolean  |      |      | 只能赋予true和flase,不进行类型转换                           |                                |

2. 案例

   ```java]
   char a = '42';
   System.out.println(a);//输出42
   char a = 42;
   System.out.println(a);//输出42对应值
   char a = "42";
   System.out.println(a);//输出字符串42
   ```

### 2.运算规则

- 整数相除时,小数位舍去;
- 整数直接量可以直接赋值给byte,short,char类型;
- 运算时超出数据类型的范围会溢出;
- byte,short,char型数据参与运算时,先一律转换为int再运算(比int类型位数低的);
- 直接写代表的是默认值,因此需要强转,而+=默认强转

### 3.类型转换

- byte<short<int<long<float<double<char

- 自动转换(从小到大)

- 强制转换(从大到小)

  (数据类型)(a+b);

## 3.分支结构

## 4.循环结构

### 1.三目

?相当于if|:相当于else if

```java
a>100?b=100:a<100?b=200:300
```

### 2.if

!flag相当于flag=false

```java
if(!flag){
    
}
```

### 3.循环控制

| 循环控制 | 作用                                            | 注意事项                           |
| -------- | ----------------------------------------------- | ---------------------------------- |
| a:       | 标识符:写在外层循环前面,循环控制后写标识符名`a` | 写了标识符下列可对外层循环进行控制 |
| return   | 退出所有循环                                    | 可以有返回值                       |
| break    | 退出本次循环                                    | 1.可以紧跟标识符;2.建议与if一起用  |
| continue | 忽略本次循环剩下的内容                          | 1.可以紧跟标识符;2.必须与if一起用  |

## 5.数组

### 1.基础知识

1. 缺点:

   1. 数组定长不可变(复制新数组);
   2. 不能保存有映射关系的数组;

2. 声明方式:

   ```java
   Int[] arr;
   Int arr[];//不推荐
   ```

### 2. 初始化

1. 动态初始化:在声明时赋`默认值`

   1. 声明:

      ```java
      int[] arr = new int[3];
      ```

   2. 赋值:

      ```java
      arr[0]=1;arrr[1]=1;arrr[2]=1;
      ```

2. 静态初始化:在声明时赋值

   1. 方法一:

      ```java
      int[] arr = new int[]{2,5,8};
      ```

   2. 方法二:必须在声明时初始化

      ```java
      int[] arr = {2,5,8};
      ```


3. 默认值:

   声明数组后若不初始化,则赋予默认值;

   | 类型     | 默认值 |
   | -------- | ------ |
   | int      | 0      |
   | double   | 0.0    |
   | boolean  | False  |
   | 引用类型 | null   |

### 3.使用

1. 数组长度

   ```java
   int x = arr.length();
   ```

2. 数组的编历

   ```java
   for(int i;i<arr.length;i++){
   	System.out.print(arr[i]);
   }
   ```

3. 数组下标越界异常(java.ArrayIndexOutOfBpundsException:N)

   当使用和赋值超过数组下标的大小时就会发生数组下标越界异常

### 4.案例

 1. 数组的复制：

    1. System.arraycopy()

       用于将一个数组的部分值复制到一个存在的数组中;

       ```java
       //将数组arr1从0开始的3个元素复制到arr2从1开始的3个位置上
       int[] arr1 = {1,2,3,4,5};
       int[] arr2 = new int[5];
       System.arraycopy(arr1,0,arr2,1,3);
       ```

    2. Arrays.copyOf()

       赋值产生新数组

       ```java
       //用arr1的前n个元素复制出一个新的数组arr2,不够的用默认值补齐,多出来的删除
       int[] arr1 = [{1,2,3,4,5};
       int[]  arr2 = Arrays.copyOf(arr1,n);
       ```

2. 扩容

   ```java
   int[] arr3=Arrays.copyOf(arr,arr.length+1);
   ```

 3. 数组的排序：

    1. 比较各种方法所需时间的方法

       ```java
       long a = System.curretTimeMillis();
       //排序命令
       long b  = System.curretTimeMillis();
       System.out.println(b-a);
       ```

    2. Arrays.sort(arr);最快//计算升序（用于数组）；

    3. 冒泡排序：从第一个元素开始到倒数第二个元素终止，与剩下的数进行比较，移位