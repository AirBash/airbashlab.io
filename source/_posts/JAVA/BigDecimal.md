---
title: BigDecimal
cover_picture: ../images/logo/JavaLogo.jpg
p: /JAVA/BigDecimal
date: 2019-11-01 20:16:38
tags:
	- JAVA
categories:
   - JAVA
---

1. 案例:

   double和float类型会发生精度丢失

   ```java
   syso(0.01+0.01);
   ```

2. 相关方法

   1. 新建对象

      1. 基于String的构造器

         ```java
         BigDecimal d1 = new BigDecimal("0.01");
         ```

      2. 静态ValueOf方法

         ```java
         BigDecimal d2 = BigDecimal.ValueOf("0.01");
         ```

   2. 计算

      1. 加

         ```java
         BigDecimal d = d1.add(d2);
         ```

      2. 减

         ```java
         BigDecimal d = d1.subtract(d2);
         ```

      3. 乘

         ```java
         BigDecimal d = d1.multiply(d2);
         
         ```

      4. 除

         1. 小数点都输出[不推荐/不常用]默认四舍五入

            ```java
            BigDecimal d = d1.divide(d2);
            
            ```

         2. 固定小数点到第n位:默认四舍五入

            ```java
            BigDecimal d = d1.divide(d2,n);
            ```

         3. 四舍五入方法

            ```java
            BigDecimal d = d1.divide(d2,n,舍入模式);
            ```

         4. 舍入模式

            | BigDecimal[淘汰不推荐] | RoundingMode | 作用                                | 个人理解   |
            | ---------------------- | ------------ | ----------------------------------- | ---------- |
            | ROUND_UP               | UP           | 始终对非零舍弃部分前面的数字加1     |            |
            | ROUND_DOWN             | DOWN         | 从不对舍弃部分前面的数字加1，即截短 |            |
            | ROUND_CEILING          | CEILING      | 正数:ROUND_UP/负数:ROUND_DOWN       | 利益最大化 |
            | ROUND_FLOOR            | FLOOR        | 负数:ROUND_UP/正数:ROUND_DOWN       |            |
            | ROUND_HALF_UP          | HALF_UP      | 四舍五入                            |            |
            | ROUND_HALF_DOWN        | HALF_DOWN    | 五舍六入                            |            |
            | ROUND_HALF_EVEN        | HALF_EVEN    | 1.5>2 1.25>1.5                      | 美国方法   |
            | ROUND_UNNECESSARY      | UNNECESSARY  |                                     |            |
