---
title: Spring基础
cover_picture: ../images/logo/SpringLogo.png
p: Spring/Spring基础
date: 2019-11-02 20:11:32
tags:
   - Spring
   - JAVA
   - 框架
categories:
   - Spring
---

### 1.基础知识

#### 1.基础配置

1. 添加依赖:

   pom.xml:测试类,不包含mvc功能

   ```xml
   <dependencies>
     	<dependency>
     		<groupId>org.springframework</groupId>
     		<artifactId>spring-context</artifactId>
     		<version>4.3.9.RELEASE</version>
     	</dependency>
   </dependencies>
   ```

2. 复制spring(applicationContext).xml文件

#### 2.注意事项

- 接口---父类---子类

  ```java
  1.接口
      ApplicationContext ac = new ClassPathXmlApplicationContext("Sping.xml文件名");//不能close
  
  2.接口实现类(父类)
      AbstractApplicationContext ac = new ClassPathXmlApplicationContext("Sping.xml文件名");
  
  3.子类(原始)
      ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("Sping.xml文件名");
  ```

- 第一个单词+简写(alt+/)

- 尽量使用方法不使用强转,而使用方法,因为方法可能会升级

- 运用反射的技术进行操作:反射无视访问权限

2. 控制反转
   1. 控制反转:Spring获取已经创建好的对象;(可以创建对象，称为Spring控制对象，也称为IOC控制反转.)
   2. 主动控制:直接new对象;

### 2.Spring IOC容器

使用IOC实例化javaBean的流程

#### 1.创建javaBean:实体类

1. 创建步骤

   1. 必须在包(package)内创建
   2. 实现Serializable序列化接口(暗中添加三个方法)
   3. 书写无参构造器(如果有构造器的话)
      1. 可以是默认构造器
      2. 方便子类的继承
   4. 创建get/set/toString定义"Bean属性";

2. java

   ```java
   class User{
       String Name;//实例变量:对象属性
       public String getName(){}//Bean属性:name
       public String setName(){}//Bean属性:name
       toString();
   }
   ```

#### 2.实例化(创建javaBean对象)

让Spring框架在启动的时候自动创建DemoBean的实例,并绑定ID为demoBean

id与name功能一样

##### 1.xml标签

- Bean标签列表

  | 代码                    | 功能                                                   | 注意事项                                                     |
  | ----------------------- | ------------------------------------------------------ | ------------------------------------------------------------ |
  | class                   | javaBean文件所在位置                                   |                                                              |
  | id                      | javaBean被实例化时的名称                               | 与name功能相同;                                              |
  | name                    | javaBean被实例化时的名称                               | 与id功能相同;                                                |
  | factory-method="方法"   | 工厂方法初始化时执行方法                               |                                                              |
  | factory-bean="Bean对象" | 工厂方法初始化Bean创建对象                             |                                                              |
  | init-method             | 初始化方法                                             | Spring会在容器启动时候，创建Bean对象，并且执行init()方法     |
  | destroy-method          | 销毁方法                                               | Spring会在关闭容器时候，销毁Bean对象，并且执行destroy()方法/仅在单例时有效 |
  | scope="prototype"       | 建立多个Bean实例,每次调用GetBean时候都会创建一个新对象 | 默认情况下,Spring中Bean是"单例"的(自始而终,singletion,并且创建的对象时同一对象) |
  | lazy-init="true";       | 懒惰初始化                                             | 适合很少使用的对象                                           |

  单例的Java在容器启动时立即初始化,反应快,性能好,占用资源高.

  懒惰初始化不是立即生效

- alias标签:设置别名

  ```xml
  <alias name="Bean名" alias ="别名" />//没有id选项只有name选项
  ```

##### 2.Spring IOC创建对象三种方法

​	只能对spring.xml配置的文件进行实例化,不能实例化@server相关的

1. 使用无参数构造器创建对象：

   ```xml
   <bean id="demoBean" class="day01.DemoBean"></bean>
   ```

  2. 调用类的静态工厂方法创建对象：

     ```xml
     <bean id="calender" class="java.util.Calendar" factory-method="getInstance"></bean>
     ```

  3. 调用对象的工厂方法创建对象：

     1. 调用了2中的Bean[不推荐]

        ```xml
        <bean id="date" factory-bean="calender" factory-method="getTime"></bean>
        ```

     2. 直接书写

        ```java
        <bean id="userServiceImpl" class="cn.tedu.service.UserServiceImpl"></bean>
        	//<constructor-arg type = "String"  value = "key" />传递参数
        <bean id="user" factory-bean="userServiceImpl" factory-method="findAll"/></bean>
        ```

#### 3.调用

1. 导入Spring配置文件(applicationContext文件,启动、初始化Spring容器)

   ```java
   AbstractApplicationContext ac = new ClassPathXmlApplicationContext("spring配置文件");
   ```

2. 从xml中获取指定ID的对象

   1. 方法一[不推荐]:

      ```java
      ID名类型 id = (ID名类型)ctx.getBean("ID名");//默认输出project类型,强转成id名对应的类型
      ```

   2. 方法二:

      ```java
      ctx.getBean("ID名",ID名类型.class);
      ```


### 3.Spring注入

#### 1.Set注入

##### 1.注入属性的值

- 注意事项java:

  1. 无参构造器的优先级小于set注入的值
  2. public时可以直接User.name,或者用private然后get方法导出来
  3. 直接输出对象时输出的是toString方法

- 注意事项property:

  2. XML文件中的property中name与java中setName()有关.set方法的参数名无关

  3. java中的set方法名通常和属性名相同

- java文件

  ```java
  public class User {
      public String name;
      public void set+A变量名(String name) {
          this.name = name;
      }
  }
  ```

- 初始化变量(相当于在set方法传入)

  ```xml
  <Bean id="" class="">
      <property name="a变量名" value="变量值"></property>
  </Bean>
  ```

##### 1.2注入非基本值

- 基础知识

  如果某属性的值不是基本值（可以直接在代码中书写的值，例如数值、字符串），而是另一个`<bean>`时，需要使用`ref`来确定该属性的值，例如：

- java

  ```java
  public Department department;
  public Department setDepartment() {
  	return department;
  }
  ```

- xml

  ```xml
  <bean id="department" class="cn.tedu.spring.department">
      <property name="x变量名" value="变量值"></property>
  </bean>
  <bean id="user" class="cn.tedu.spring.User">
      <property name="department" ref="department" />
  </bean>
  
  ```

##### 2.通过有参构造方法注入属性的值[不常用]

- 注意事项:

  - ~~通常情况下都不需要有参构造器,正常的类都需要无参构造器;~~

- 如果某个类没有无参数的构造方法，且参数可以用于属性赋值.

  - 同理，如果构造方法的参数的类型不是基本值能表达的，则应该通过`ref`来注入值！

  - `index`是从0开始顺序编号的！

- java文件

  ```java
  public class Person {
      public String name; // Mike
      public Person(String name) {
          super();
          this.name = name;
      }
  }
  ```

- 则在Spring的配置文件中：

  ```xml
  <bean id="person" class="cn.tedu.spring.Person">
      <!-- index：为第几个参数注入值 -->
      <constructor-arg index="0" value="Mike"></constructor-arg>
  </bean>
  ```

##### 3.自动装配非基本值:autowire[不常用]

- 注意事项:

  - 不建议使用,容易出现错误

  - 自动装配是Spring自动的为属性注入值的做法，也是基于SET注入方式实现的;
  - byName:根据set名称装配:
    - 名称很容易保持一致
  - byType:根据属性类型装配:
    - 1.没有匹配的对象不会自动配置也不会发生错误;
    - 2.需要保证匹配类型的对象有且仅有1个

- xml1

  ```xml
  <bean id="user" class="cn.tedu.spring.User" autowire="byName"></bean>
  <bean id="user" class="cn.tedu.spring.department"></bean>
  ```

- xml2

  ```xml
  <bean id="user" class="cn.tedu.spring.User" autowire="byType"></bean>
  <bean id="user" class="cn.tedu.spring.department"></bean>
  ```

- 多对象错误

  ```错误
  Caused by: org.springframework.beans.factory.NoUniqueBeanDefinitionException: No qualifying bean of type 'cn.tedu.spring.UserDao' available: expected single matching bean but found 2: userDao,userDao2
  ```

##### 4.注入集合类型

**1.List/Set**[仅了解]

- 注意事项

  - set在spring中是:LinkedHashSet;
  - list早spring中是:ArrayList;

- java

  ```java
  public List<String> names;
  public void setNames(List<String> names) {
  	this.names = names;
  }
  ```

- xml

  ```xml
  <bean id="" class="">
  	<property name="names">
  		<list>
  			<value>1</value>
  			<value>2</value>
  			<value>3</value>
  		</list>
  	</property>
  </bean>
  ```

**2.map**[仅了解]

- 注意事项:LinkedHashMap

- java

  ```java
  public Map<String,Object> session;
  public void setSession(Map<String,Object> session){
  	this.session = session;
  }
  ```

- xml

  ```xml
  <bean id="" class="">
  	<property name="session">
  		<map>
  			<entry key="username" value="root" />
  			<entry key="password" value="ad" />
  			<entry key="age" value="18" />
  		</map>
  	</property>
  </bean>
  ```

**3.数组**[仅了解]

- 注意事项:

  数组的配置可以和list混为一谈(set/list/array标签可以互相使用)

- java

  ```java
  public String[] classes;
  public void setClasses(String[] classes) {
  	this.classes = classes;
  }
  ```

- xml

  ```xml
  <list><!--<array><set>-->
  	<value>JSD1811</value>
  	<value>JSD1812</value>
  	<value>JSD1901</value>
  </list>
  ```

- test

  ```java
  SampleBean sampleBean = ac.getBean("sampleBean", SampleBean.class);
  System.out.println(sampleBean.classes.getClass());
  //不能直接输出,直接输出输出的是类型
  System.out.println(Arrays.toString(sampleBean.classes));
  ```

**4.properties**[掌握]

- 注意事项:

  - properties存在的原因:相比较于xml可读性更高
  - spring配置文件使用properties方法:

- properties

  ```properties
  url=jdbc:mysql://localhost:3306/db_name?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai
  driver=com.mysql.jdbc.Driver
  username=root
  password=root
  initialSize=2
  maxActive=10
  ```

- java

  ```java
  public Properties dbConfig;
  public void setDbConfig(Properties dbConfig) {
  	this.dbConfig = dbConfig;
  }
  ```

- xml

  - 方法1[不推荐]:

    ```xml
    <bean id="" class="">
    	<property name="dbConfig">
    		<props>
    			<prop key="driver">com.mysql.jdbc.Driver</prop>
                ...
    		</props>
    	</property>
    </bean>
    ```

  - 方法2:

    - \<util>相当于bean
    - 还可以用\<util>配置其他的集合

    ```xml
    <bean id="" class="">
    	<property name="dbConfig" ref="db" />
    </bean>
    <util:properties id="db" location="classpath:db.properties"></util:properties>
    ```

#### 2.Spring表达式

用于获取非基本值

- 获取非基本值(取代ref)

  #{bean_id.属性名}

  ```xml
  <bean id="" class="">
  	<property name="username" value="#{user.name}"></property>
  </bean>
  ```

- 获取List/Set集合[不推荐]

  #{bean_id.集合属性名[索引值]}

  - 注意事项
    - []内的是索引,并不是数组,因此在大多数情况下不建议使用;
    - set不能使用:因为他没有索引(但是Spring中linkedHashSet可以)

  ```xml
  <bean id="" class="">
  	<property name="username" value="#{sampleBean.names[1]}"></property>
  </bean>
  ```

- 获取Map集合

  1.#{bean_id.map属性名.key}

  2.#{bean_id.map属性名['key']}

  推荐第一个因为可以少写代码

  ```xml
  <bean id="" class="">
  	<property name="password" value="#{sampleBean.session.password}"></property>
  </bean>
  ```

#### 3.注解注入

- 扫描注解
- 在Spring加载时执行只会影响启动速度,但是并不会影响使用

##### 1.componet注解:

- 原理:

  Spring会扫描包名内所有的class,如果配置了Component注解就会创建对象(包名>=注解所在包)

- 分类

  以下注解的功能是完全等效的,但是应该区分使用:

  | 注解        | 名称       |
  | ----------- | ---------- |
  | @Component  | 通用注解   |
  | @Controller | 控制类注解 |
  | @Service    | 业务类注解 |
  | @Repository | 持久层类   |

- 配置

  1. xml

     @Controller注解扫描

     ```xml
     <context:component-scan base-package="包名" />
     ```

   2. java

   - 使用默认Bean-id(类名首字母变小写,与之前相同)

     ```java
     @Component
     public class 类名{
     }
     ```

    - 自定义Bean-id[不常用]

      ```java
      @Component("id名")
      public class 类名{
      }
      ```


##### 2.其他注解[不常用]

1. **多例**

   ```java
   @Scope("prototype")//多例
   ```

2. **懒加载**

   ```java
   @Lazy
   @Lazy(true)
   ```

3. **单例**[不推荐]

   3.2**非懒加载**(默认)

   - 初始化时立即生效

   - java

     ```java
     public class King {
         private static King king = new King();
         //2.因为要返回同一个对象,因此不能直接返回new对象;
         private King() {}
         //1.将构造方法私有化,这样不能在外界new对象
         public static King getInstance() {
         //3.设置成static,可以不创建对象直接调用方法
         //4.static执行级别高,因此提前加载进内存,而这里返回的King对象没有加载进去,因此这个King也要设置成static;static一直在内存中,因此取决于使用频率;
             return king;
         }
     }
     ```

   - 单例注解

     ```java
     @Scope("singleton")//默认单例无需添加
     @Scope//单例
     ```

   - 非懒加载

     ```java
     @Lazy(false)
     ```

   3.2 **懒加载**

   - 初始化时不生效,使用时启动(但是初始化一次)

   - java

     ```java
     public class King {
         private static King king = null;
         private King() {}
         public static King getInstance() {
             if(king==null){//判断是否需要锁
                 sychronized("java"){//线程安全问题,两个线程同时进入可能会创建多个对象
                     if(king==null){//判断是否创建对象
                     king = new King();
            		}
             }
             return king;
         }
     }
     ```

4. **生命周期**

   在使用这2个注解之前，需要添加Tomcat运行环境，否则这2个注解将无法识别。tomcat负责这两个操作,但是可以在junit中使用

   ```java
   @PostConstruct 初始化
   @PreDestroy 结束时
   ```

5. **自动装配非基本属性**

   - 注意事项:

     1. 写在需要自动装配的属性上面,不写在类上面;

     2. 先根据byName(set)如果失败再根据byType

   - 方法1:

     ```java
     public class abc(){
         @Component
         public efgh efg;
     }
     ```

   - 方法2:

     注意：`@Resource`是`javax`包中的，如果使用该注解，需要项目添加Tomcat环境。

     ```java
     public class abc(){
        	@Resource(name="efgh")
         public efgh efg;
     }
     ```
