---
title: SpringBoot
cover_picture: ../images/logo/SpringLogo.png
p: Spring/SpringBoot
date: 2019-11-02 20:45:20
tags:
   - SpringBoot
   - JAVA
   - 框架
categories:
   - Spring
---

### 1.基础配置

它是一个默认添加了许多依赖，完成了许多(公共)默认的配置，集成了主流的Spring+SpringMVC的框架。

#### 1. 创建项目

1. 打开网址 http://start.spring.io;
2. 填写参数,选择额外依赖,点击Generate Project按钮
3. 下载项目压缩包
4. 解压压缩包并剪切到eclipse项目目录中
5. eclipse中导入项目:**Import**> **Existing Maven Projects**,并自动下载依赖;

#### 2. maven配置

1. 自动添加

   ```xml
   <!--SpringBoot默认:配置MVC或Mybatis时会被替换-->
   <dependency>
   	<groupId>org.springframework.boot</groupId>
   	<artifactId>spring-boot-starter</artifactId>
   </dependency>
   <!--SpringBoot默认:junit下列框架默认都有-->
   <dependency>
   	<groupId>org.springframework.boot</groupId>
   	<artifactId>spring-boot-starter-test</artifactId>
   	<scope>test</scope>
   </dependency>
   
   <!--mysql/jdbc驱动-->
   <dependency>
   	<groupId>mysql</groupId>
   	<artifactId>mysql-connector-java</artifactId>
   	<scope>runtime</scope>
   </dependency>
   
   <!--SpringMVC:替换Spring-starter-->
   <dependency>
   	<groupId>org.springframework.boot</groupId>
   	<artifactId>spring-boot-starter-web</artifactId>
   	</dependency>
   <dependency>
   
   <!--Mybatis:替换Spring-starter-->
   <dependency>
   	<groupId>org.mybatis.spring.boot</groupId>
   	<artifactId>mybatis-spring-boot-starter</artifactId>
   	<version>2.1.0</version>
   </dependency>
   ```

#### 3. 默认配置

1. 推荐把静态资源(html/图片/css/js素材)放到resources/static文件夹中
2. java文件夹中默认创建了`cn.tedu.groupiD名.项目名`包,Spring专属包;并在该包创建`项目名Application.java文件`,该类时启动类

3. 项目内置独立Tomcat,并且不用部署,使用时直接执行启动类就行,直接访问http://localhost:8080/
4. 默认配置了DispatcherServlet,处理路径是/*,后缀无(任意都行)
5. 默认配置了StringHtppMessageConverter,字符集UTF-8
6. 默认添加了jackson依赖,boot推荐服务器这样做,利于多客户端使用:@RestController=@ResponseBody+@Controller

### 2. 项目规范

1. 在java文件中创建包

   | 包名        | 存储类型                    |
   | ----------- | --------------------------- |
   | controller  | controller                  |
   | entity      | 实体类                      |
   | mapper      | 接口类                      |
   | util        | 工具类(返回类/实体类的一种) |
   | interceptor | 拦截器                      |
   | conf        | 拦截器String类              |

2. 在resource文件中创建目录

   | 目录名  | 存储类型     |
   | ------- | ------------ |
   | mappers | 接口配置文件 |

### 4.JUnit

1. **基础配置**[不推荐]:

   此处建议直接复制生成的测试文件,就不用了设置了

   - 调用Spring文件

     ```java
     @RunWitch(SpringRunner.class);
     ```

   - 其他

     ```java
     @SpringBootTest
     ```

2. **自动装配对象**

   无需new就可以自动声明

   ```java
   @Autowired
   UserMapper mapper;
   ```

3. **报错解决**

   1. 步骤文件中`@AutoWired`错误或`@MapperScan`出错,导致找不到接口类;

      ```error
      Caused by: org.springframework.beans.factory.NoSuchBeanDefinitionException: No qualifying bean of type 'cn.tedu.store.mapper.UserMapper' available: expected at least 1 bean which qualifies as autowire candidate. Dependency annotations: {@org.springframework.beans.factory.annotation.Autowired(required=true)}
      ```

   2. Mapper.xml中 `id`或者`namespace`属性出错,导致查找不到接口类的方法

      ```error
      org.apache.ibatis.binding.BindingException: Invalid bound statement (not found): cn.tedu.store.mapper.UserMapper.insert
      ```

   3. Mapper.xml中`@param`定义的和引用的数量不一样导致数组下标越界

      ```error
      org.mybatis.spring.MyBatisSystemException: nested exception is org.apache.ibatis.binding.BindingException: Parameter '#modifiedUser' not found. Available parameters are [modifiedTime, modifiedUser, param3, aid, param1, param2]
      ```

      ```error
      Caused by: java.sql.SQLException: Parameter index out of range (1 > number o)
      ```

   4. mapper.xml中sql语句字段名出错

      一大坨

      ```html
      org.springframework.jdbc.BadSqlGrammarException: 
      ### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column '错误字段名' in 'field list'<!--错误的字段名-->
      ### The error may exist in file [D:\tts9\workspace\store\target\classes\mappers\UserMapper.xml]<!--错误文件所在系统位置-->
      ```

   5. mapper.xml中sql语句出错

      一大坨

      ```html
      org.springframework.jdbc.BadSqlGrammarException: 
      ### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '' at line 1<!--错误的位置-->
      ### The error may exist in file [D:\eclipse_workspace\store09\target\classes\mappers\AddressMapper.xml]<!--错误文件在系统中的位置-->
      ```

   6. Mapper.java中方法出错

      ```error
      Caused by: java.lang.ClassNotFoundException: Cannot find class: 
      ```

   7. 实体类中的某个属性错误

      ```eror
      Caused by: org.apache.ibatis.reflection.ReflectionException: There is no getter for property named 'phone' in 'class cn.tedu.store.entity.User'
      ```

   8. junit报错,报错台无信息

      测试类包名出错或者忘记写@Test

### 5.连接数据库

1. **添加依赖**

   此时不添加数据源启动就会失败

   ```xml
   <dependency>
   	<groupId>org.mybatis.spring.boot</groupId>
   	<artifactId>mybatis-spring-boot-starter</artifactId>
   	<version>2.0.0</version>
   </dependency>
   
   <dependency>
   	<groupId>mysql</groupId>
   	<artifactId>mysql-connector-java</artifactId>
   	<scope>runtime</scope>
   </dependency>
   ```

2. **配置数据源**

   配置resources下的application.properties文件

   ```properties
   spring.datasource.url=jdbc:mysql://localhost:3306/tedu_ums?useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai
   spring.datasource.username=root
   spring.datasource.password=root
   ```

3. **Junn测试**

   1. 用junit运行测试文件

      在配置完数据源后,自动在test文件件中创建`项目名ApplicationTest.java`文件

      主要用于检测依赖是否正常,正常的话不报错

   2. 自行测试

      1. 复制java文件

      2. test

         ```java
         @AutoWired
         DataSource datesource;
         Connection conn = dataSource.getConnection();
         syse(conn);
         ```

### 6.使用MyBatis

1. **调用mapper.xml**

   acclication.properties

   ```properties
   mybatis.mapper-locations=classpath:mappers/*.xml
   ```

2. **创建实体类**

   1. 可用于MyBatis的数据库使用
   2. 可用于AJAX做返回值使用

   ```java
   public class User {
   	private String username;
   	private String password;
   	private Integer age;
   	private String phone;
   	private String email;
   	创建get/set/toString方法
   }
   ```

3. **扫描接口类**

   1. 在接口类配置[不推荐,每一个都要配置]

      ```java
      @Mapper
      public interface UserMapper {
      }
      ```

   2. 启动类配置

      ```java
      @MapperScan("cn.tedu.boot.demo.mapper[,包2]")//要扫描的包,类似spring.xml
      public class DemoApplication{
      }
      ```


4. **配置SQL语句**


   1. 使用注解[不推荐,不利于维护]

      ```java
      @insert(SQL语句)
      Integer insert(User user);
      ```

   2. 使用XML

      跟MyBatis配置方式相同,在resources下新建mapper.xml文件

      mapper.xml

      ```xml
      <mapper namespace="cn.tedu.boot.demo.mapper.UserMapper">
      	<insert id="insert"
         		useGeneratedKeys="true" keyProperty="id"> INSERT INTO t_user (username,password,age,phone,email,is_delete)VALUES(#{username},#{password},#{age},#{phone},#{email},#{isDelete})
         	</insert>
      </mapper>
      ```

   3. **测试**

      ```java
      @Test
      public void insert(){
          @Autowired
      	UserMapper mapper;
      	User user = new User();
      	user.setUsername("springboot");
      	user.setPassword("8888");
      	Integer rows = mapper.insert(user);
      	System.err.println("rows"+rows);
      }
      ```

### 7.拦截器

1. 新建拦截器类

   指定拦截的规则:不符合规则的拦截,并转发到指定位置

   ```java
   public class LoginInterceptor implements HandlerInterceptor {//1.实现HandlerInterceptor类
   	@Override//2.生成preHandle方法
   	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)throws Exception {
   		if(uid==null){
   			return false;
   		}
   		return true;
   	}
   }
   ```

2. 新建配置类

   白名单和黑名单都可以传入List类型

   ```java
   public class A implements WebMvcConfigurer{//1.实现WebMvcConfigurer类
       	@Override//2.实现addIntercdptors方法
   	public void addInterceptors(InterceptorRegistry registry) {
   		HandlerInterceptor interceptor=new LoginInterceptor();//新建拦截器类对象
   	registry.addInterceptor(interceptor)
       .addPathPatterns("/**")//白名单
       .excludePathPatterns(patterns);//黑名单
   /*		InterceptorRegistration x = registry.addInterceptor(interceptor);
   		x.addPathPatterns(patterns);
   		x.excludePathPatterns(patterns);*/
   	}
   }
   ```

### 8.实例流程

下面配置建立在上面配置完数据库的情况下

1. 创建实体类

   见上文`使用MyBatis--创建实体类`

2. 创建接口

   UserMapper.java

   ```java
   public interface UserMapper(){
       User findByUsername(String username);
   }
   ```

3. 创建返回类(工具类)

   ResponseResult.java

   ```java
   private Integer state;
   private String message;
   生成get/set/toString方法
   生成无参构造器:防止框架找不到无参构造器报错
   生成有参数构造器:用于做返回类使用;
   生成只有state的有参构造器:值返回一个值时使用(灵活性,建议使用)
   ```

4. 创建Mapper文件

   UserMapper.xml

   ```xml
   <mapper>
   	<select resultType="cn.tedu.boot.demo.entity.User">
           SELECT id FROM t_user WHERE username = #{username}
       </select>
   </mapper>
   ```

5. 修改Application.properties

   见上文`使用MyBatis`

6. 创建登录网页

   reg.html

   ```html
   <form>
       <h1>用户注册</h1>
       <div>请输入用户名</div>
       <div><input name="username"></div>
       <div><span id="username-hint"></span></div>
       <div>请输入密码</div>
       <div><input name="password"></div>
       <div><span id="password-hint"></span></div>
       <div>请输入年龄</div>
       <div><input name="age"></div>
       <div>请输入手机号码</div>
       <div><input name="phone"></div>
       <div>请输入电子邮箱</div>
       <div><input name="email"></div>
       <div><input id="btn-reg" type="button" value="注册"></div>
   </form>
   <script type="text/javascript" src="jquery-1.8.3.min.js"></script>
   <script type="text/javascript">
   	$("btn-reg").click(function(){
          	$("#username-hint").html("");//防止第二次校验失效
   		$("#password-hint").html("");
           $ajax({
               "url":"user/reg",
               "data":$("#from-reg").serialize(),
               "type":"post",
               "dataType":"json",
               "success":function(json){
                   if(json.state==1){
                       alert("注册成功");
                   }else if(json.state==2){
                    	$("#username-hint").html(json.message);
                   }else{//此处无需设置
                       $("#password-hint").html(json.message);
                   }
               }
           })
       })
   </script>
   ```

7. 创建controller

   Controller.java

   ```java
   @RestControlle//设置ajax:相当于不返回给处理器;而返回给发送数据的html文件
   @RequestMapping("user")
   public class UserController{
       @Autowired//自动装配接口类,MVC不需要调用Spring文件
       private UserMapper userMapper
   	@RequestMapping("reg")
   	public String reg(User user){//传入了一个User,前面都没有类似操作,可以直接传入数据
           User user = userMapper.finByUsername();
           if(user == null){//测试是要注册的用户名是否已存在
               return new ResponseResult(1);//只有state的构造器
           }else{
               return new ResponseResult(2),"用户名已经被占用";
           }
   	}
   }
   ```

8. 直接访问127.0.0.1:8080即可