---
title: Maven
cover_picture: ../images/logo/MavenLogo.jpg
p: /版本管理/Maven
date: 2019-11-01 21:51:58
tags:
	- Maven
	- 版本管理
categories:
   - Apache
---

## 1.基础内容

### 1.基础配置

1. 默认配置文件

   不相同部分,以局部配置为准

   1. 全局配置文件:maven/conf/setting.xml
   2. 局部配置文件:user/.m2/setting.xml

2. 默认存储位置:user/.m2/repository

3. 配置本地仓库:修改配置文件

   ```xml
   <localRpository>仓库地址</localRpository>
   ```

### 2.基础知识

#### 1.标准目录结构

| 目录               | 内容         |
| ------------------ | ------------ |
| src/main/java      | 核心代码部分 |
| src/main/resources | 配置文件部分 |
| src/test/java      | 测试代码部分 |
| src/test/resouce   | 测试配置文件 |
| src/main/webapp    | 页面资源     |

#### 2.生命周期

| 生命周期     | 流程                                | 注意事项                             |
| ------------ | ----------------------------------- | ------------------------------------ |
| 清理生命周期 | clean                               |                                      |
| 默认生命周期 | compile/test/package/insgall/deploy | 执行后面的命令时自动执行前面所有命令 |
| 结算生命周期 |                                     |                                      |

编译--->测试--->打包--->安装--->发布

## 2.相关命令

1. 查看当前tomcat版本

   ```maven
   mvn -v[ersion]
   ```

2. 删除编译

   删除`target`目录,用于编译环境不同

   ```maven
   mvn clean
   ```

3. 编译

   编译`main`

   ```maven
   mvn compile
   ```

4. 测试

   编译`main`和`test`

   ```maven
   mvn test
   ```

5. 打包

   编译`main`和`test`并根据pom.xml中的`<packing>`配置进行打包

   ```mavenn
   mvn package
   ```

6. 安装

   安装到本地仓库,原理先执行`mvn package`

   ```maven
   mvn install
   ```

7. 使用tomcat运行

   默认tomcat6不支持java8.0,要先配置tomcat7插件

   ```maven
   mvn tomcat[版本]:run
   ```

## 3.pom.xml配置

### 1.依赖配置

#### 1.依赖范围

| 依赖范围 | 编译有效 | 测试有效 | 运行有效 | 例子        |
| -------- | -------- | -------- | -------- | ----------- |
| compile  | ✔        | ✔        | ✔        | 默认/大多数 |
| test     |          | ✔        |          | junit       |
| provided | ✔        | ✔        |          | servlet     |
| runtime  |          | ✔        | ✔        | jdbc        |
| system   | ✔        | ✔        |          | maven       |

在依赖上配置,设置为仅在编译时启用

```xml
<scope>provided</scope>
```

### 2.插件配置

#### 1.修改jdk版本

1. 方法1

   ```xml
   <build>  
   	<plugins>  
   		<plugin>  
   			<groupId>org.apache.maven.plugins</groupId>  
   			<artifactId>maven-compiler-plugin</artifactId>  
   			<configuration>  
   				<source>1.8</source>  
   				<target>1.8</target>
                   <encoding>UTF-8</encoding>
   			</configuration>  
   		</plugin>  
   	</plugins>  
   </build>
   ```

2. 方法2

   ```xml
   <properties>
   	<maven.compiler.source>1.8</maven.compiler.source>
   	<maven.compiler.target>1.8</maven.compiler.target>
   	<maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
   	<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
   	<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
   </properties>
   ```

#### 2.修改tomcat插件

[官方版本号](https://tomcat.apache.org/maven-plugin.html)

```xml
<build>
	<plugins>
		<plugin>
			<groupId>org.apache.tomcat.maven</groupId>
			<artifactId>tomcat7-maven-plugin</artifactId>
            <version>2.1</version><!--2.1或2.2均可-->
            <configuration>
                <server>服务器名称</server>
            	<port>8080</port>
            	<path>/访问地址</path>
                <username>tomcat</username>
				<password>tomcat</password>
            	<uriEncoding>UTF-8</uriEncoding>
            </configuration>
           </plugin>
	</plugins>
</build>
```

## 4.IDEA

### 1.基础配置

1. 整合maven

   设置--->`构建/部署`--->`构建工具`--->`maven`--->maven home--->配置maven根目录

2. 使用本地骨架:

   设置--->`构建/部署`--->`构建工具`--->`maven`-->`runner`--->VM Options--->`-DarchetypeCatalog=internal`

### 2.基础操作

#### 1.新建maven项目

在使用骨架创建maven项目时有的目录需要手动添加,建议不使用骨架,而直接创建原生maven项目,创建成功后会提示`build success`

1. 标准项目

   `文件`--->`new`--->`项目`--->`maven`--->下一个--->填写相关信息

2. java骨架

   在`maven`中勾选`create from archetype`--->选择`org.apache.maven.archetypes:maven-archetype-quickstart`

3. web骨架

   在`maven`中勾选`create from archetype`--->选择`org.apache.maven.archetypes:maven-archetype-webapp`

#### 2.设置目录为web目录

设置后可以新建web相关的文件

`文件`--->`项目结构`--->`模块`--->选择要编辑的项目中的web(没有的话右击添加web)--->将目录添加进去

#### 3.没有新建servlet文件

`文件`--->`项目结构`--->`模块`--->选择要编辑的项