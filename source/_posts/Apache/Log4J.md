---
title: Log4J
cover_picture: ../images/logo/Log4JLogo.png
p: JavaAPI/Log4J
date: 2019-11-02 21:10:15
tags:
   - Apache
   - JAVA
   - log4j
categories:
   - Apache
---

## 1.基础知识

1. 作用:

   记录项目产生的日志:调试日志/运行日志/异常日志

2. 实现方式:

   1. JDK的logger
   2. log4j和log4j2

3. 日志级别

   | 日志  | 级别    | 作用 |
   | ----- | ------- | ---- |
   | fatal | 1(最高) | 致命 |
   | error | 2       |      |
   | warn  | 3       |      |
   | info  | 4       |      |
   | debug | 5       | 调试 |
   | trace | 6       | 堆栈 |

## 2. 基础配置

#### 1.添加依赖

1. pom.xml

   ```xml
   <dependency>
       <groupId>log4j</groupId>
       <artifactId>log4j</artifactId>
       <version>1.2.17</version>
   </dependency>
   ```

2. 调价报错依赖

   SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
   SLF4J: Defaulting to no-operation (NOP) logger implementation
   SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.

   ```xml
   <dependency>
   	<groupId>org.slf4j</groupId>
   	<artifactId>slf4j-nop</artifactId>
   	<version>1.7.2</version>
   </dependency>
   ```

#### 2.配置文件

1. log4j.properties

   ```properties
   log4j.rootLogger=DEBUG,Console,File,DailyRollingFile,RollingFile
      
   #Console 
   log4j.appender.Console=org.apache.log4j.ConsoleAppender  
   log4j.appender.Console.layout=org.apache.log4j.PatternLayout  
   log4j.appender.Console.layout.ConversionPattern=%d [%t] %-5p [%c] - %m%n
      
   #File
   log4j.appender.File = org.apache.log4j.FileAppender
   log4j.appender.File.File = D://log1.log
   log4j.appender.File.layout = org.apache.log4j.PatternLayout
   log4j.appender.File.layout.ConversionPattern =%d [%t] %-5p [%c] - %m%n
   log4j.appender.File.Append = false
   
   #DailyRollingFile
   log4j.appender.DailyRollingFile = org.apache.log4j.DailyRollingFileAppender
   log4j.appender.DailyRollingFile.File = D://log2.log
   log4j.appender.DailyRollingFile.layout = org.apache.log4j.PatternLayout
   log4j.appender.DailyRollingFile.layout.ConversionPattern =%d [%t] %-5p [%c] - %m%n
   
   #RollingFile
   log4j.appender.RollingFile = org.apache.log4j.RollingFileAppender
   log4j.appender.RollingFile.File = D://log3.log
   log4j.appender.RollingFile.MaxFileSize=1KB
   log4j.appender.RollingFile.MaxBackupIndex=3
   log4j.appender.RollingFile.layout = org.apache.log4j.PatternLayout
   log4j.appender.RollingFile.layout.ConversionPattern =%d [%t] %-5p [%c] - %m%n
   ```

2. ##### 详解

   1. 输出等级

      ```properties
      #将DEBUG及以上日志使用输出类型输出
      #OFF(0)<FIFAL<ERROR<WARN<INFO<DEBUG<ALL(全部)
      log4j.rootLogger=DEBUG,输出类型1[,输出类型2]
      ```

   2. 输出格式:

      log4j.appender.输出名称.layout=org.apache.log4j.PatternLayout推荐配置

      | 格式名称                       | 作用                                   | 注意事项 |
      | ------------------------------ | -------------------------------------- | -------- |
      | org.apache.log4j.HTMLLayout    | 以HTML表格形式布局                     |          |
      | org.apache.log4j.PatternLayout | 可以灵活地指定布局模式                 |          |
      | org.apache.log4j.SimpleLayout  | 包含日志信息的级别和信息字符串         |          |
      | org.apache.log4j.TTCCLayout    | 包含日志产生的时间、线程、类别等等信息 |          |

   3. 自定义配置

      log4j.appender.输出名称.layout.ConversionPattern=%d [%t] %-5p [%c] - %m%n推荐配置

      | 配置类型 | 配置名称                                                     | 注意事项 |
      | -------- | ------------------------------------------------------------ | -------- |
      | %m       | 输出代码中指定的消息；                                       |          |
      | %M       | 输出打印该条日志的方法名；                                   |          |
      | %p       | 输出优先级，即DEBUG，[INFO](http://baike.baidu.com/item/INFO)，WARN，ERROR，FATAL； |          |
      | %r       | 输出自应用启动到输出该log信息耗费的毫秒数；                  |          |
      | %c       | 输出所属的类目，通常就是所在类的全名；                       |          |
      | %t       | 输出产生该日志事件的线程名；                                 |          |
      | %n       | 输出一个回车换行符，Windows平台为"rn”，Unix平台为"n”；       |          |
      | %d       | 输出日志时间点的日期或时间，默认格式为ISO8601，也可以在其后指定格式，比如：%d{yyyy-MM-dd HH:mm:ss,SSS}，输出类似：2002-10-18 22:10:28,921； |          |
      | %l       | 输出日志事件的发生位置，及在代码中的行数                     |          |

   4. 输出类型

      log4j.appender.输出类型=org.apache.log4j.输出类型Appender

      | 输出类型         | 作用                                   | 注意事项 |
      | ---------------- | -------------------------------------- | -------- |
      | Console          | 控制台                                 |          |
      | File             | 只产生一个日志文件                     |          |
      | DailyRollingFile | 每天产生一个日志文件                   |          |
      | RollingFile      | 文件大小超过指定大小时产生新文件       |          |
      | WriterAppender   | 将日志信息以流格式发送到任意指定的地方 |          |

   5. 输出名称

      | 输出类型         | 输出名称                                                     |
      | ---------------- | ------------------------------------------------------------ |
      | File             | 默认续写                                                     |
      | DailyRollingFile | 最后一天的名称与输出名称相同,之前的为            输出名称+日期 |
      | RollingFile      | 当超过配置的制定大小时创建新文件,文件名为     输出名称n      |

      ```properties
      log4j.appender.输出类型.File = D://log.log
      ```

   6. 额外配置

      ```properties
      #File 当配置false时不续写,直接覆盖
      log4j.appender.File.append = false
      
      #DailyRollingFile
      
      # RollingFile:当文件大小超过1KB时新建新文件,当文件数超过3个时覆盖之前的文件
      log4j.appender.RollingFile.MaxFileSize=1KB
      log4j.appender.RollingFile.MaxBackupIndex=3
      ```

#### 3.java配置

1. 新建logger事例

   ```java
   Logger logger = Logger.getLogger(当前.class);
   ```

2. 格式化logger

   ```java
   logger.日志级别名("显示信息");
   ```

3. 格式化错误日志

   ```java
   logger.日志级别名("显示信息",new 错误);
   ```

## 3.框架配置

#### 1.Spring MVC

1. web.xml

   ```xml
   <!-- 加载log4j的配置文件log4j.properties -->
   <context-param>
   	<param-name>log4jConfigLocation</param-name>
   	<param-value>classpath:log4j.properties</param-value>
   </context-param>
   <!-- 设定刷新日志配置文件的时间间隔，这里设置为10s -->
   <context-param>
   	<param-name>log4jRefreshInterval</param-name>
   	<param-value>10000</param-value>
   </context-param>
   <!-- 加载Spring框架中的log4j监听器Log4jConfigListener -->
   <listener>
   	<listener-class>org.springframework.web.util.Log4jConfigListener</listener-class>
   </listener>
   <!-- 为避免项目间冲突，定义唯一的 webAppRootKey -->
   <context-param>
   	<param-name>webAppRootKey</param-name>
   	<param-value>scheduleProject</param-value>
   </context-param>
   ```

#### 2.Spring Boot

直接使用即可