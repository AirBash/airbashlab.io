---
title: Lucene
cover_picture: ../images/logo/LuceneLogo.jpg
p: 搜索引擎/Lucene
date: 2019-11-02 21:02:40
tags:
	- Apache
	- Lucene
	- JAVA
categories:
   - Apache
---

## 1.基础知识

### 1.相关依赖

#### 1.官网依赖包

[官方下载](https://lucene.apache.org/)

#### 2.maven

多个maven包版本号要相同

```xml
<dependency>
	<groupId>org.apache.lucene</groupId>
	<artifactId>包名</artifactId>
	<version>版本号</version>
</dependency>
```

| 包名                    | 作用     |      |
| ----------------------- | -------- | ---- |
| lucene-core             | 核心包   |      |
| lucene-analyzers-common | 分词功能 |      |
| lucene-queryparser      | 分词查询 |      |

### 2.Luke可视化工具

在`Lucene`8.0.0以后的版本中官方包中自带Luke,官方原[官方项目](https://code.google.com/archive/p/luke/)托管已停止更新,8.0.0以前的可查看[现官方项目](https://github.com/DmitryKey/luke/);

## 2.Lucene核心类

| 创建        | 作用       | 查询          | 作用       |
| ----------- | ---------- | ------------- | ---------- |
| IndexWriter | 写入索引流 | IndexReader   | 读取索引流 |
| Analyzer    | 分析器     | Query         | 查询方式   |
| Document    | 文档       | indexSearcher | 搜索       |
| Field       | 域         | Term          | 类似域     |

| 其他类        | 作用 |      |
| ------------- | ---- | ---- |
| Directory     |      |      |
| TokenStream   |      |      |
| TermAttribute |      |      |

### 1.创建索引

#### 1.Analyer

| 分词器           | 详解                                                    |
| ---------------- | ------------------------------------------------------- |
| StandardAnalyzer | 默认分词器,中文词会被拆分成单个字                       |
| IKAnalyzer       | 中文分词器,官方项目已停止更新,可用别人开发的项目(maven) |
|                  |                                                         |

#### 2.Document

文档对象包含域

![](imgs/lucene文档模型.png)

#### 3.Field

| Field类型                                                    | 数据类型               | Analyzed   是否分析 | Indexed   是否索引 | Stored   是否存储 | 说明                                                         |
| ------------------------------------------------------------ | ---------------------- | ------------------- | ------------------ | ----------------- | ------------------------------------------------------------ |
| StringField(FieldName,   FieldValue,Filed.Store.YES/NO))     | 字符串                 | N                   | Y                  | Y或N              | 这个Field用来构建一个字符串Field，但是不会进行分析，会将整个串存储在索引中，比如(订单号,姓名等) |
| LongPoint/IntPoint(String name, long... point)               | Long型                 | Y                   | Y                  | N                 | 可以使用LongPoint、IntPoint等类型存储数值类型的数据。让数值类型可以进行索引。但是不能存储数据，如果想存储数据还需要与StoredField一起使用。 |
| StoredField(FieldName, FieldValue)                           | 重载方法，支持多种类型 | N                   | N                  | Y                 | 这个Field用来构建不同类型Field   不分析，不索引，但要Field存储在文档中,可以使用LongPoint以后在使用这个进行存储 |
| TextField(FieldName, FieldValue, Filed.Store.YES/NO)  或 TextField(FieldName, reader) | 字符串   或   流       | Y                   | Y                  | Y或N              | 如果是一个Reader, Lucene猜测内容比较多,会采用Unstored的策略. |

文件的大小/内容/名称/地址都可以加入域中

### 2.查询索引

#### 1.Query

| 搜索方式    | 作用                 |
| ----------- | -------------------- |
| TermQuery   | 根据域名和关键词搜索 |
| RanageQuery | 根据范围查询         |
|             |                      |

#### 2.TopDocs

| 方法/属性     | 作用                   | 返回值                        |
| ------------- | ---------------------- | ----------------------------- |
| totalHits     | 符合搜索条件的文档数量 | TotalHits对象 (输出为:n hits) |
| socreDocs     | 符合搜索条件的结果     | scoreDoc数组                  |
| getMaxScore() | 返回最大评分           |                               |
| TopDocs()     | 排序(多个重载方法)     |                               |

##### 1.ScoreDoc

| 方法/属性  | 作用           |
| ---------- | -------------- |
| score      | 文档的查询分数 |
| doc        | 文档编号       |
| shardIndex | 未知/不常用    |

##### 2.TotalHits

| 方法/属性 | 作用                  |
| --------- | --------------------- |
| value     | 输出数值n(Int)        |
| relation  | 命中范围(未知/不常用) |

## 3.执行流程

### 1.创建索引

1. 指定索引库保存位置

   ```java
   Path path = new File("地址").toPath();
   Path path = Paths.get("地址);
   Directory directory = FSDirectory.open(path);//需要抛出IO异常
   ```

2. 新建分词器

   ```java
   Analyzer analyzer = new 分词器名();
   ```

3. 新建写入索引配置对象

   ```java
   IndexWriterConfig config= new IndexWriterConfig(analyzer);//不写参数代表默认配置
   ```

4. 新建写入索引对象

   ```java
   IndexWriter indexWriter = new IndexWriter(directory,config);
   ```

5. 创建文档对象

   ```java
   Document document = new Document();//建议1个文档对应所有类型的域各一个
   ```

6. 创建域

   文件的大小/内容/名称/地址都可以加入域中

   ```java
   Field field = new 域类型("域名","内容",Filed.Store.YES);//yes代表存储,no代表不存储.
   ```

7. 添加一条记录

   ```java
   document.add(field);
   ```

8. 将记录添加到索引事例中

   ```java
   indexWriter.addDocument(document);
   ```

9. 关闭索引事例

   ```java
   indexWriter.close();
   ```

### 2.演示分词效果

1. 新建分词对象

   ```java
   Analyzer anlyzer= new 分词对象();
   ```

2. 创建标记流,测试域名

   ```java
   TokenStream tokenStream = anlyzer.tokenStream("域名","事例文字或者流");//域名可不写
   ```

3. 添加功能

   | 名称                       | 作用                                                         | 注意事项                                                     |
   | :------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
   | CharTermAttribute          | 表示token本身的内容                                          |                                                              |
   | OffsetAttribute            | 表示token的首字母和尾字母在原文本中的位置                    | 首字母:startOffset()/尾字母:startOffset()                    |
   | TypeAttribute              | 表示token的词汇类型信息，默认值为word，其它值有\<ALPHANUM> \<APOSTROPHE> \<ACRONYM>\<COMPANY> \<EMAIL> \<HOST> \<NUM> \<CJ><ACRONYM_DEP> |                                                              |
   | FlagsAttribute             | 与TypeAttribute类似，假设你需要给token添加额外的信息，而且希望该信息可以通过分析链，那么就可以通过flags去传递 |                                                              |
   | PositionIncrementAttribute | 表示当前token相对于前一个token的相对位置，也就是相隔的词语数量,如果两者之间没有停用词，那么该值默认为1 | 例如“text for attribute”，text和attribute之间的getPositionIncrement为2 |
   | PayloadAttribute           | 在每个索引位置都存储了payload（关键信息），当使用基于Payload的查询时，该信息在评分中非常有用 |                                                              |

   ```java
   功能类 x = tokenStream.addAttribute(功能名.class);
   ```

4. 重置标记流

   ```java
   tokenStream.rest();//会抛出IOException
   ```

5. 遍历tokenStream

   ```java
   while(tokenStream.incrementToken(){//返回flase表示结束
       sout(CharTermAttribute.toString());
   }
   ```

6. 关闭标记流

   ```java
   tokenStream.end();//待学
   tokenStream.close();
   ```

### 3.查询索引

#### 1.Query子类

1. 指定索引库位置

   ```java
   Directory directory = FSDirectory.open(path);
   ```

2. 创建读取索引事例

   ```java
   IndexReader indexReader = DirectoryReader.open(directory);
   ```

3. 创建搜索对象

   ```java
   IndexSearcher indexSearcher = new IndexSearcher(indexReader);
   ```

4. 指定搜索方式

   ```java
   Query query = 搜索方式(term);
   Query query = new TermQuery(new Term("搜索域","关键词"));//1.TermQuery
   LongPoint.RangeQuery("搜索域",startL,endL);//2.RangeQuery
   ```

5. 设置query和显示个数并返回结果

   ```java
   TopDocs topDocs = indexSearcher.search(query,10);
   ```

6. 显示搜索结果的信息集数组

   ```java
   ScoreDoc[] scoreDocs = topDocs.scoreDocs;
   ```

7. 显示结果

   ```java
   for(ScoreDoc scoreDoc:scoreDocs){   
   	int docId = scoreDoc.doc;							 //信息集的id
   	Document document = indexSearcher.doc(docId);		 //通过id获取文档
   	System.out.println(document.get("域名"));			   //根据域名查询内容
   }
   ```

#### 2.QueryParser

修改第4部分

```
//1.创建分词器
Analyzer analyzer = new IKAnalyzer();
//2.创建QueryParaser对象
QueryParser queryParser = new QueryParser("name", analyzer);
//3.创建Query
query = queryParser.parse("lucene是一个java开发的全文检索工具包");
```

### 4.管理索引库

#### 1.添加索引

跟添加索引一样的操作

#### 2.删除索引

指定索引库-->1/2--->结束

##### 1.删除所有索引

```java
indexWriter.deleteAll();
```

##### 2.根据查询删除索引

```java
indexWriter,deleteDocuments(new Term("域名","关键词"));
```

#### 3.修改索引

跟添加索引库一样只不过`document.add()`修改为下列代码

1. 修改后的文档

   ```java
   document.add(new TextField("要修改的域名","修改后的内容",存储类型))
   ```

2. 根据搜索关键词替换document

   ```java
   indexWriter.updateDocument(new Term("域名","关键词"),document);
   ```