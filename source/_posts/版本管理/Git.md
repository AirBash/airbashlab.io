---
title: Git
cover_picture: ../images/logo/GitLogo.jpg
p: /版本管理/Git
date: 2019-11-01 20:24:04
tags:
   - Git
   - 版本管理
categories:
	- 版本管理
---

## 1.基础介绍

### 1.详情

1. 基础知识: 分布式管理系统,与之相对应的是svn集中式管理系统

2. [汉化gui教程]( https://jingyan.baidu.com/article/ae97a64602a606bbfd461d93.html);

3. 环境变量: 默认自动修改

### 2.快捷键

| 快捷键 | 名称 |
| ------ | ---- |
| ctrl+l | 清屏 |

### 3.版本分支流程

1. 相关信息

   | 英文名  | 中文名         | 作用(流程见流程图) |
   | ------- | -------------- | ------------------ |
   | master  | 主干分支       |                    |
   | develop | 开发分支       |                    |
   | hotfix  | bug修理分支    |                    |
   | release | 发行前测试分支 |                    |

2. 流程图

   ![](imgs/git版本流程.png)

### 4.新建项目流程

1. 新建目录(库),在目录内新建文件,右击git输入`git init`命令
2. 打开`github`---project创建库和项目
3. 打开项目复制项目网页上的库地址(`ssh`或`https`)
4. 建立github和本地库的联系
5. 添加到缓存区--->本地仓库-->-远程

## 2.命令

### 1.基础配置

- 基础配置:

  - 每次git提交时都会引用这两条信息,以说明是谁提交了更新,并永久纳入历史记录

  - 下列是全局配置,当版本库没有配置时启用
  - --global:全局配置,不写就是配置当前版本库

1. 设置个人邮箱

   ```git
   git config [--global] user.email "294379349@qq.com"
   ```

2. 设置个人用户名

   ```git
   git config [--global] user.name "zsp"
   ```

3. 设置默认文本编辑器

   subl 配置了sublime 可以使用subls

   ```git
   git config [--goble] core.editor 名称
   ```

4. 配置完成后,自动在/用户目录下生成`/.gitconfig`文件,里面保存着1-3设置的内容,也可以直接修改该文件

### 2.SSH秘钥登录

1. 生成秘钥

   默认保存在 /用户名/.ssh中

   ```git
   ssh-keygen -t rsa -C 注释
   ```

2. 打开`github`---settings,添加`ssh`秘钥,将.ssh中的公钥写入(注意删除末尾空格)

3. 验证连通性

   成功的话会在`.ssh`目录下生成` known_hosts`文件,并提示`access`

   ```linux
   ssh -T git@github.com
   ```

### 3.仓库命令

1. 创建仓库

   在工作目录中执行,成功后会生成`.git`文件

   ```git
   git init [目录]
   ```

2. 将本地文件添加到暂存区

   `.`:所有文件;` *.java`:java文件

   ```git
   git add 文件名
   ```

3. 将缓存区文件推送到本地仓库

   ```git
   git commit [-m "注释"]
   ```

4. 建立远程库和本地库的联系

   ```git
   git remote add origin 项目地址
   ```

5. 将本地仓库推送到远程仓库

   - 第一次推送

     ```git
     git push -u origin master
     ```

   - 第二次推送

     ```git
     git push origin 版本
     ```

6. 将远程仓库下载到本地仓库

   直接clone就不需要`init`和`remote`命令,因为该命令相当于创建并建立关系

   - 第一次下载

     111.230.13.216 

     ```git
     git clone 地址
     ```

   - 普通下载

     ```git
     git pull
     ```

### 4.排除文件

| 范围     | 配置文件                          | 优缺点              |
| -------- | --------------------------------- | ------------------- |
| 全局     | 位置自定义/,gitignore             | 操作简单,仅个人可用 |
| 局部     | 项目/.git/.gitignore              | 所有人可用          |
| 局部本地 | 项目/.git/info/exclude/.gitignore | 仅本地单个项目可用  |

1. 指定全局配置文件

   ```git
   git config --global core.excludesfile /指定目录/.gitignore
   ```

2. 从版本控制中取消并add

   ```git
   git rm -r --cached .
   git add .
   ```

   | 组件     | 作用                      |
   | -------- | ------------------------- |
   | -r       | 递归删除(与linux相似)     |
   | --cached | 不删除文件,仅从索引中删除 |

## 3.Eclipse

### 1.安装配置

1. 下载`egit`插件
2. 打开`windows`---`Preferences`
3. 选择`Team`---`Git`
   1. `Git`---Default repository folder:配置默认clone目录,这里设置为工作目录
   2. `Configuration`---User Settings---Location:配置默认git配置文件`.gitconfig`(可以在此处修改email和name)
4. 配置SSH私钥目录:选择`General`---`Network Connections`---`SSH2`---General---SSH2 home:

### 2.新建库

1. git init

   1. 复制项目到指定库中:右击项目`Team`---`ShareProject`---Create---选择要生成到的目录上
   2. 将项目变成库:右击项目`Team`---`ShareProject`,勾选Use or create repository in parent folder of project,点击Create Repository

2. git add .

   右击项目`Team`---`add to index`

3. git commit

   1. 右击项目`Team`---`commit`---弹出窗口---书写注释---commit
   2. 右击项目`Team`---`commit`---弹出窗口---书写注释---commit and push(会让你执行以下操作)

4. git  remote add origin 项目地址&git push

   右击项目`Team`---`Remote`---`push`---URL:地址---source ref:设置分支类型---点击add Spec---next

### 3.下载库

1. `new`---`other`---git---clone

2. 右击项目---Team--pull

### 4.冲突解决

1. 查看冲突:

   右击项目---`Team`---`synchonrized`

2. 处理冲突:

   1. 工具处理:右击项目---`Tream`---`marge Tool`
   2. 直接处理:
      1. 本地冲突文件:git add---git commit---git push移动到本地仓库
      2. 远程冲突文件:git pull覆盖本地文件
      3. 比较二者的区别

### 5.版本分支

1. 新建分支
   1. 右击项目--->`Team`--->`Switch to`--->`new Branch`--->弹出窗口
   2. Select:选择要克隆的分支--->Branch name:设置分支名称
2. 合并分支