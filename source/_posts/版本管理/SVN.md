---
title: SVN
cover_picture: ../images/logo/SVNLogo.jpg
p: /版本管理/SVN
date: 2019-11-01 21:33:12
tags:
   - SVN
   - 版本管理
categories: 
   - 版本管理
---
# 二.SVN

## 1.安装

### 1.安装服务端

建议安装[visualSVN server](https://www.visualsvn.com/downloads/)图形化操作软件,或者官网推荐软件[subvison](https://sourceforge.net/projects/win32svn/)

### 2.安装客户端

建议安装[TortoiseSVN](https://tortoisesvn.net/downloads.html)图形化操作软件和其对应的[汉化包](https://tortoisesvn.net/downloads.html)

## 2.Eclipse

### 1.相关配置

#### 1.安装svn

下载`subclise`插件;

#### 2.断开svn连接

​	断开连接:右击项目>>>`team`>>>`断开连接`>>>勾选删除云数据(不作为svn)

#### 3.删除已保存资源库位置:

​	svn资源库>>>删除多余位置

#### 4.将svn插件设置为英文

1. 打开`eclipse所在目录\configuration\config.ini文件`

2. 增加英文配置:

   ```ini
   # Set Subversion English Version
   osgi.nl=en_US
   ```

#### 5.查看svn记录

打开svn记录视图

### 2.SVN操作

#### 1.提交项目

1. 右击项目>>>`team`>>>`share`>>>输入svn仓库地址>>>在不输入密码的情况下要点击finsh4-5次
2. 右击项目>>>`team`>>>`commit`

#### 2.下载项目

1. 打开svn视图:`windows`>>>`show view`>>>`other`>>>svn资源库

2. 添加资源库:右击svn资源库(或者点击视图右上角图标)>>>添加新的资源库>>>填写url
3. check out:右击资源库>>>检出为..>>>作为工作空间中的项目检出[>>>修改项目名称>>>选择保存到的位置]
4. 手动导入该项目