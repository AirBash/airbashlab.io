---
title: IDEA设置注释模板
cover_picture: ../images/logo/IDEA.png
date: 2019-11-28 22:26:34
tags:
    - IDEA
    - IDE
categories:
    - IDE
---
IDEA 中设置注释模板主要分为两个部分，分别是创建 java 文件时类的注释和方法的注释,大家可以根据自己的习惯生成自己喜欢的注释模板,效果如下：

![](https://img2018.cnblogs.com/blog/711223/201812/711223-20181214210320564-898214189.jpg)

**一、设置类的注释模板**

1、选择 File→Settings→Editor→File and Code Templates→Files→Class。可以看到创建 Class 时引入了一个参数 "File Header.java"。对应的是 Files 旁边的Includes→File Header 文件。

![](https://img2018.cnblogs.com/blog/711223/201812/711223-20181214195032832-1568050227.jpg)

2、File Header 里就是创建类时的注释模板，下面 Description 中有描述一些可以配置的参数，可以根据自己需要选用。

![](https://img2018.cnblogs.com/blog/711223/201812/711223-20181214195120383-81812013.jpg)

**二、设置方法的注释模板**

1、选择 File→Settings→Editor→Live Templates。点击右边的加号，选择 Template Group，创建一个分组。

![](https://img2018.cnblogs.com/blog/711223/201812/711223-20181214200908620-704042505.jpg)

2、再点击加号，选择 Live Template，创建一个模板。其中：

Abbreviation：填模板的缩写，可以使用 * 号作为代号，方便后面调用模板。

Options→Expand with：填注释模板的扩展快捷键，根据使用习惯，这里使用默认的 Tab。

Template text：填写注释模板的内容。参数名使用 ${参数名}$ 的格式。**（注意：注释模板开头的 / 不要填写，原因可以看文章末尾的解释）**

![](https://img2018.cnblogs.com/blog/711223/201812/711223-20181214201740869-2072625082.jpg)

3、点击模板页面最下方的警告 define，来设置将模板应用于哪些场景，选择 Everywhere-->Java 即可。（如果曾经修改过，则显示为 change 而不是 define，如上图）

![](https://img2018.cnblogs.com/blog/711223/201812/711223-20181214202201097-27072621.jpg)

4、然后点击 Edit variables，会读取刚刚在注释模板中的配置的参数 ${参数名}$。在 Expression 的下拉框中选择方法为其赋值。

![](https://img2018.cnblogs.com/blog/711223/201812/711223-20181214202605565-65328838.jpg)

5、参数赋值优化

（1）如果 param 参数使用默认的 methodParameters() 来获取方法参数值，其注释参数是在一行展示的，如下图。这里我们希望像 MyEclipse 中一样，一行一个参数的样式，可以通过脚本来实现。将以下脚本复制进 Expression，可以得到文章开头效果图的样式：

groovyScript("def result=''; def params=\"${_1}\".replaceAll('[\\\\[|\\\\]|\\\\s]', '').split(',').toList(); for(i = 0; i < params.size(); i++) {result+=' * @param '+ params[i] + ((i < params.size() - 1) ?'\\r\\n':'')}; return result", methodParameters())

![](https://img2018.cnblogs.com/blog/711223/201812/711223-20181214203205908-258980306.jpg)

（2）如果 return 参数使用默认的 methodReturnType() 来获取参数值，模板里使用了 @link 来跳转结果类型，那么当方法返回值为 void 时，注释会报错，如下图。可以通过脚本来避免，将上面模板中的 @link 参数去掉，将以下脚本复制进 Expression：

groovyScript("def result=\"${_1}\"; if(result == \"void\"){return \"\";}else{return \"{@link \"+result+\"}\";}", methodReturnType())

![](https://img2018.cnblogs.com/blog/711223/201812/711223-20181217105813710-466662842.jpg)

6、注释模板的调用。通过输入 / + * + Tab 键，即先输 /* 再按 Tab 键，就可以调用模板。因为刚刚我们把 * 被设置为模板的代号，Tab 键设置为模板的扩展快捷键，也可以把 Tab 改为 Enter 键，更加还原 MyEclipse。

**PS**:（1）注意，注释模板开头不要使用 /，因为设置后虽然可以更便捷的使用 * + Tab 键，调用模板。但是会存在 param 为 null 的情况（原因暂时未知），如下图，需要在方法内调用才能获取参数，但这样还需要将注释复制到方法外反而使用不便。

![](https://img2018.cnblogs.com/blog/711223/201812/711223-20181214205654274-569030714.jpg)

（2）注释模板中的 user 参数是获取系统的用户（当然注释作者也可以直接写固定值，但是配置更有意思，哈哈），经常不是自己需要的作者名，可以在 IDEA 中进行配置。打开→IDEA 的安装目录 \ bin\idea64.exe.vmoptions。在最下面增加一行 - Duser.name=username。

其中 username 就是你希望为 user 参数的赋值。